<?php


namespace App\Enums;


interface TransactionType
{
    const BONUS = 'bonus';
    const CHARGE = 'charge';
    const DEPOSIT = 'deposit';
    const INTEREST = 'interest';
    const WITHDRAW = 'withdraw';
    const REFERRAL = 'referral';
    const TRANSFER = 'transfer';
}

<?php

namespace App\Http\Controllers\API;

use Modules\BasicKYC\BasicKYCModule;
use Modules\BasicKYC\Helpers\ImportSettings;
use App\Jobs\ProcessEmail;
use App\Traits\WrapInTransaction;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Setting;
use App\Models\UserKycQuestion;
use App\Http\Controllers\Controller;
use App\Helpers\ResponseHelper;

use Modules\BasicKYC\Helpers\KycStatus;
use Modules\BasicKYC\Helpers\KycSessionStatus;

use Modules\BasicKYC\Models\KycSessions;
use Modules\BasicKYC\Models\KycApplicants;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class KycController extends Controller
{
    private $kycModule = NULL;
    private $pathTemp = NULL;
    private $pathSave = NULL;

    public function __construct(BasicKYCModule $module)
    {
        $this->kycModule = $module;
        $this->pathTemp = "kyc-temp";
        $this->pathKyc = "kyc-section";
        $this->pathSave = "kyc";
        $this->responseHelper = new ResponseHelper;
    }

    private function finalPreview()
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession)) {
            $main = $getSession->document("main");
            $bs = $getSession->document("bs");
            $ub = $getSession->document("ub");
            if (!$this->isProfileInfoExist($getSession->profile)) {
                return $this->basicInfo();
            }
            if (!$this->hasDocsFiles($main)) {
                return $this->documents();
            }
            $session_id = data_get($getSession, "session");

            $data = [
                "data" => $getSession, 
                "main" => $main, 
                "bs" => $bs, 
                "ub" => $ub, 
                "session" => $session_id
            ];
            return response()->json($this->responseHelper->successWithData($data), 200);
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function submitKyc(Request $request)
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $userId) {
                    $user = User::findOrFail(auth()->user()->id);
                    // $this->moveSubmittedFiles($getSession);
                    $getKycApplicant = KycApplicants::where("user_id", $userId)->first();
                    $getKycApplicant->status = KycStatus::PENDING;
                    $getKycApplicant->save();
                    $getSession->created_at = Carbon::now();
                    $getSession->status = KycSessionStatus::COMPLETED;
                    $getSession->save();
                    UserMeta::updateOrCreate(["user_id" => $userId, "meta_key" => "kyc_verification"], ["meta_value" => "pending"]);
                    Session::put("basic_complete", "no");
                    $success = module_msg_of("success", "status", "BasicKYC");
                    ProcessEmail::dispatch('kyc-submission-user', $user);
                    ProcessEmail::dispatch('kyc-submission-admin', $user);
                    return response()->json($this->responseHelper->successWithoutData('Our team will review your account and approve it etc etc'), 200);
                }, $getSession, $userId);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function cancelKyc()
    {
        $getSession = KycSessions::started()->where("user_id", auth()->user()->id)->first();
        $this->removeSubmittedFiles($getSession);
        Session::put("basic_complete", "no");
        return response()->json($this->responseHelper->successWithoutData('KYC has been cancelled'), 200);
    }
    public function statusKyc()
    {
        $getStatus = KycApplicants::where("user_id", auth()->user()->id)->first();
        if (!$getStatus) {
            return response()->json($this->responseHelper->errorCustom(422, "status not found."), 422);
        }
        return response()->json($this->responseHelper->successWithData($getStatus), 200);
    }
    private function isProfileInfoExist($profileData)
    {
        $profileFields = Arr::get(gss("kyc_fields"), "profile");
        foreach ($profileFields as $key => $item) {
            if (Arr::get($item, "show") == "yes" && Arr::get($item, "req") == "yes") {
                switch ($key) {
                    case "address":
                        if (!Arr::get($profileData, "address_line_1") || !Arr::get($profileData, "state")) {
                            return false;
                        }
                        break;
                    case "country":
                        if (!Arr::get($profileData, "country")) {
                            return false;
                        }
                        break;
                    case "nationality":
                        if (!Arr::get($profileData, "nationality")) {
                            return false;
                        }
                        break;
                    case "gender":
                        if (!Arr::get($profileData, "gender")) {
                            return false;
                        }
                        break;
                    case "phone":
                        if (!Arr::get($profileData, "phone")) {
                            return false;
                        }
                        break;
                    case "dob":
                        if (!Arr::get($profileData, "dob")) {
                            return false;
                        }
                        break;
                    case "name":
                        if (!Arr::get($profileData, "name")) {
                            return false;
                        }
                        break;
                }
            }
        }
        return true;
    }
    private function getBasicInfo()
    {
        $userId = auth()->user()->id;
        $userMeta = UserMeta::where("user_id", $userId)->pluck("meta_value", "meta_key")->toArray();
        if (!empty($userMeta)) {
            $userMeta = array_filter($userMeta);
        }
        $kycProfile = KycSessions::started()->where("user_id", $userId)->first();
        if (!empty($kycProfile["profile"])) {
            $kycProfile = array_filter($kycProfile["profile"]);
        }
        $nationality = Arr::get($userMeta, "profile_nationality") == "same" ? Arr::get($userMeta, "profile_country") : Arr::get($userMeta, "profile_nationality");
        $result = [
            "first_name" => Arr::get($kycProfile, "first_name", auth()->user()->name),
            "last_name" => Arr::get($kycProfile, "last_name", auth()->user()->name),
            "gender" => Arr::get($kycProfile, "gender", Arr::get($userMeta, "profile_gender")),
            "dob" => Arr::get($kycProfile, "dob", Arr::get($userMeta, "profile_dob")),
            "phone" => Arr::get($kycProfile, "phone", Arr::get($userMeta,"profile_phone")),
            "nationality" => Arr::get($kycProfile, "nationality", $nationality),
            "country_of_residence" => Arr::get($kycProfile, "country_of_residence", Arr::get($userMeta, "profile_country_of_residence")),
            "year_in_country" => Arr::get($kycProfile, "year_in_country", Arr::get($userMeta, "profile_year_in_country")),
            // "address" => Arr::get($kycProfile, "address", Arr::get($userMeta, "profile_address")),
            "address_line_1" => Arr::get($kycProfile, "address_line_1", Arr::get($userMeta, "profile_address_line_1")),
            "address_line_2" => Arr::get($kycProfile, "address_line_2", Arr::get($userMeta, "profile_address_line_2")),
            "city" => Arr::get($kycProfile, "city", Arr::get($userMeta, "profile_city")),
            // "state" => Arr::get($kycProfile, "state", Arr::get($userMeta, "profile_state")),
            "zip" => Arr::get($kycProfile, "zip", Arr::get($userMeta, "profile_zip")),
            "twitter" => Arr::get($kycProfile, "twitter", Arr::get($userMeta, "profile_twitter")),
            "facebook" => Arr::get($kycProfile, "facebook", Arr::get($userMeta, "profile_facebook")),
            "instagram" => Arr::get($kycProfile, "instagram", Arr::get($userMeta, "profile_instagram")),
            "tiktok" => Arr::get($kycProfile, "tiktok", Arr::get($userMeta, "profile_tiktok")),
            "education" => Arr::get($kycProfile, "education", Arr::get($userMeta, "profile_education")),
            "marital_status" => Arr::get($kycProfile, "marital_status", Arr::get($userMeta, "profile_marital_status")),
            "children" => Arr::get($kycProfile, "children", Arr::get($userMeta, "profile_children")),
            "monthly_income" => Arr::get($kycProfile, "monthly_income", Arr::get($userMeta, "profile_monthly_income")),
            "source_income" => Arr::get($kycProfile, "source_income", Arr::get($userMeta, "profile_source_income")),
            "monthly_liabilities" => Arr::get($kycProfile, "monthly_liabilities", Arr::get($userMeta, "profile_monthly_liabilities"))
        ];
        return $result;
    }

    public function basicInfoUpdate(Request $request)
    {
        $userId = auth()->user()->id;
        $kycApplicant = KycApplicants::where("user_id", $userId)->first();
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        $basicInfo = $this->getBasicInfo();
        if (blank($kycApplicant)) {
            $this->addNewApplicant();
        }
        if (blank($getSession)) {
            $addSession = $this->addNewSession($basicInfo);
            $getSession = $addSession;
        }
        $rules = [];
        $messages = [];
        // rules
        $rules["first_name"]            = "required|string|max:190";
        $rules["last_name"]             = "required|string|max:190";
        $rules["gender"]                = "required|string|max:100";
        $rules["dob"]                   = "required|date_format:m/d/Y";
        $rules["phone"]                 = "required|string|max:100";
        $rules["nationality"]           = "required|string|max:100";
        $rules["country_of_residence"]  = "required|string|max:100";
        $rules["year_in_country"]       = "required|string|max:100";
        $rules["address_line_1"]        = "required|string|max:100";
        $rules["address_line_2"]        = "nullable|max:100";
        $rules["city"]                  = "required|string|max:100";
        $rules["zip"]                   = "required|string|max:100";
        $rules["twitter"]               = "nullable|string|max:100";
        $rules["facebook"]              = "nullable|string|max:100";
        $rules["instagram"]             = "nullable|string|max:100";
        $rules["tiktok"]                = "nullable|string|max:100";
        $rules["education"]             = "required|string|max:100";
        $rules["marital_status"]        = "required|string|max:100";
        $rules["children"]              = "required|string|max:100";
        $rules["monthly_income"]        = "required|string|max:100";
        $rules["source_income"]         = "required|string|max:100";
        $rules["monthly_liabilities"]   = "nullable|string|max:100";

        // $messages
        $messages["first_name.required"]            = __("Please enter your full First Name.");
        $messages["last_name.required"]             = __("Please enter your full Last Name.");
        $messages["gender.required"]                = __("Please Choose your Gender.");
        $messages["dob.required"]                   = __("Please Input your Date of Birth.");
        $messages["dob.date_format"]                = __("Enter Date of Birth in this 'mm/dd/yyyy' format.");
        $messages["phone.required"]                 = __("Please Input your Phone Number.");
        $messages["nationality.required"]           = __("Please Input your nationality Number.");
        $messages["country_of_residence.required"]  = __("Please Input your Country of Residence Number.");
        $messages["year_in_country.required"]       = __("Please Input your Year in Country.");
        $messages["address_line_1.required"]        = __("Please Input your Address Line 1.");
        $messages["city.required"]                  = __("Please Choose your City.");
        $messages["zip.required"]                   = __("Please Input your Zip Code.");
        $messages["education.required"]             = __("Please Choose your Education.");
        $messages["marital_status.required"]        = __("Please Choose your Marital Status.");
        $messages["children.required"]              = __("Please Choose your Marital Status.");
        $messages["monthly_income.required"]        = __("Please Input your Monthly Income.");
        $messages["source_income.required"]         = __("Please Choose your Source Income.");
        // $messages["monthly_liabilities.required"]   = __("Please Choose your Monthly Liability.");

        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["first_name","last_name","gender","dob","phone","nationality","country_of_residence","year_in_country","address_line_1","address_line_2","city","zip","twitter","facebook","instagram","tiktok","education","marital_status","children","monthly_income","source_income","monthly_liabilities",]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();

        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    Session::put("basic_complete", "yes");
                    $session_id = data_get($getSession, "session");
                    $countries = filtered_countries();
                    return response()->json($this->responseHelper->successWithData(["basicInfo" => $this->getBasicInfo()]), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step1(Request $request)
    {
        $userId = auth()->user()->id;
        $kycApplicant = KycApplicants::where("user_id", $userId)->first();
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        $basicInfo = $this->getBasicInfo();
        if (blank($kycApplicant)) {
            $this->addNewApplicant();
        }
        if (blank($getSession)) {
            $addSession = $this->addNewSession($basicInfo);
            $getSession = $addSession;
        }
        $rules = [];
        $messages = [];
        $rules["first_name"] = "required|string|max:190";
        $rules["last_name"] = "required|string|max:190";
        $messages["first_name.required"] = __("Please enter your full First Name.");
        $messages["last_name.required"] = __("Please enter your full Last Name.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["first_name","last_name"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();

        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    $session_id = data_get($getSession, "session");
                    $countries = filtered_countries();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step2(Request $request)
    {
        $rules = [];
        $messages = [];
        $require = data_get(gss("kyc_fields"), "profile.address.req") == "yes" ? true : false;
        $rules["gender"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $messages["gender.required"] = __("Please Choose your Gender.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["gender"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step3(Request $request)
    {
        $rules = [];
        $messages = [];
        $require = data_get(gss("kyc_fields"), "profile.address.req") == "yes" ? true : false;
        $rules["dob"] = $require ? "required|date_format:m/d/Y" : "nullable|date_format:m/d/Y";
        $messages["dob.required"] = __("Please Input your Date of Birth.");
        $messages["dob.date_format"] = __("Enter Date of Birth in this 'mm/dd/yyyy' format.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["dob"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step4(Request $request)
    {
        $rules = [];
        $messages = [];
        $require = data_get(gss("kyc_fields"), "profile.address.req") == "yes" ? true : false;
        $rules["phone"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $messages["phone.required"] = __("Please Choose your Phone Number.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["phone"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step5(Request $request)
    {
        $rules = [];
        $messages = [];
        $require = data_get(gss("kyc_fields"), "profile.address.req") == "yes" ? true : false;
        $rules["nationality"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $rules["country_of_residence"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $rules["year_in_country"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $messages["nationality.required"] = __("Please Input your nationality Number.");
        $messages["country_of_residence.required"] = __("Please Input your Country of Residence Number.");
        $messages["year_in_country.required"] = __("Please Input your Year in Country.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["nationality","country_of_residence","year_in_country"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step6(Request $request)
    {
        $rules = [];
        $messages = [];
        $require = data_get(gss("kyc_fields"), "profile.address.req") == "yes" ? true : false;
        $rules["address_line_1"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $rules["address_line_2"] = $require ? "nullable|max:100" : "nullable|string|max:100";
        $rules["city"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $rules["zip"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $messages["address_line_1.required"] = __("Please Input your Address Line 1.");
        $messages["city.required"] = __("Please Choose your City.");
        $messages["zip.required"] = __("Please Input your Zip Code.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["address_line_1","address_line_2","city","zip"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step7(Request $request)
    {
        $rules = [
            "type" => "required|in:pp,nid,dvl", 
            "country" => "required|string|max:50",
            "number" => "required|string|max:50",
            "issue" => "required|date_format:m/d/Y",
            "expiry" => "required|date_format:m/d/Y",
        ];
        $messages = [
            "type.in" => __("Please select a valid document type."), 
            "type.required" => __("Please select the type of document."), 
            "country.required" => __("Please select the issued by country."),
            "number.required" => __("Please enter your document number."),
            "issue.required" => __("Please enter your issue date."),
            "expiry.required" => __("Please enter your expiry date."),
            "issue.date_format" => __("Enter issue date in this 'mm/dd/yyyy' format."),
            "expiry.date_format" => __("Enter expiry date in this 'mm/dd/yyyy' format."),
        ];
        $validatedData = $request->validate($rules, $messages);
        $docType = data_get($validatedData, "type");
        $docMeta = Arr::only($validatedData, ["country", "number", "issue", "expiry"]);
        $docMeta = array_map("strip_tags_map", $docMeta);
        $userId = auth()->user()->id;
        $input = $request->validate([
            "document" => "required_if:type,pp|file|mimetypes:image/jpg,image/png,image/jpeg|max:5200"
        ], 
        [
            "document.required" => __("File is required."), 
            "document.max" => __("File exceeds the maximum upload file size of 5 MB."), 
            "document.file" => __("Please select a valid file type and try once again."), 
            "document.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), 
            "document.uploaded" => __("Unable to upload file due to technical issues.")
        ]);

        $getSession = KycSessions::started()->where("user_id", $userId)->first();

        $basicInfo = $this->getBasicInfo();
        if (blank($getSession)) {
            $addSession = $this->addNewSession($basicInfo);
            $getSession = $addSession;
        }

        if (!blank($getSession) && data_get($getSession, "session")) {

            if($docType == 'pp')
            {
                try {
                    $hash = cipher(now()->timestamp);
                    $mainDoc = $getSession->document("main");
                    $mainFiles = data_get($mainDoc, "files", []);
                    $document = short_to_docs($docType);
                    $file = $request->document;
                    $exten = strtolower($file->getClientOriginalExtension());
                    $name = auth()->id() . "_" . $docType . "_" . $hash . "." . $exten;
                    $file->storeAs($this->pathKyc, $name);
                    $resultFile = ["main" => $name];
    
                    if (!blank($mainDoc)) {
                        $mainDoc->type = $docType;
                        $mainDoc->meta = $docMeta;
                        $mainDoc->files = $resultFile;
                        $mainDoc->save();
                    } else {
                        $kycDocs = new \Modules\BasicKYC\Models\KycDocs();
                        $kycDocs->session_id = $getSession->id;
                        $kycDocs->user_id = $getSession->user_id;
                        $kycDocs->type = $docType;
                        $kycDocs->meta = $docMeta;
                        $kycDocs->files = $resultFile;
                        $kycDocs->save();
                        $mainDoc = $kycDocs->fresh();
                    }
                    $getSession->docs = array_merge(data_get($getSession, "docs", []), ["main" => $docType]);
                    $getSession->save();
                    $session_id = data_get($getSession, "session");
                    return response()->json($this->responseHelper->successWithData(["doc" => $docType, "document" => $document, "session" => $session_id]), 200);
                } catch (\Exception $e) {
                    save_error_log($e);
                    return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
                }
            }
            else
            {

                $kycDocs = new \Modules\BasicKYC\Models\KycDocs();
                $kycDocs->session_id = $getSession->id;
                $kycDocs->user_id = $getSession->user_id;
                $kycDocs->type = $docType;
                $kycDocs->meta = $docMeta;
                // $kycDocs->files = $resultFile;
                $kycDocs->save();
                // $mainDoc = $kycDocs->fresh();

                $getSession->docs = array_merge(data_get($getSession, "docs", []), ["main" => $docType]);
                $getSession->save();
                $session_id = data_get($getSession, "session");
                return response()->json($this->responseHelper->successWithData(["doc" => $docType, "session" => $session_id]), 200);

            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step7Front(Request $request)
    {
        $userId = auth()->user()->id;
        $input = $request->validate([
            "front" => "required_if:type,nid,dvl|file|mimetypes:image/jpg,image/png,image/jpeg|max:5200",
        ], 
        [
            "front.required" => __("File is required."), 
            "front.max" => __("File exceeds the maximum upload file size of 5 MB."), 
            "front.file" => __("Please select a valid file type and try once again."), 
            "front.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), 
            "front.uploaded" => __("Unable to upload file due to technical issues."),
        ]);
        $getSession = KycSessions::started()->where("user_id", $userId)->first();

        $basicInfo = $this->getBasicInfo();
        if (blank($getSession)) {
            $addSession = $this->addNewSession($basicInfo);
            $getSession = $addSession;
        }

        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                $hash = cipher(now()->timestamp);
                $mainDoc = $getSession->document("main");
                $document = short_to_docs($mainDoc->type);
                $file = $request->front;
                $exten = strtolower($file->getClientOriginalExtension());
                $name = auth()->id() . "_" . $mainDoc->type . "_" . $hash . "." . $exten;
                $file->storeAs($this->pathKyc, $name);
                $resultFile = ["front" => $name];

                $kycDocs = new \Modules\BasicKYC\Models\KycDocs();
                $kycDocs->session_id = $getSession->id;
                $kycDocs->user_id = $getSession->user_id;
                $kycDocs->type = $mainDoc->type;
                $kycDocs->meta = $mainDoc->meta;
                $kycDocs->files = $resultFile;
                $kycDocs->save();
                // $mainDoc->files = array_merge($mainDoc->files, $resultFile);
                // $mainDoc->files = $resultFile;
                // $mainDoc->save();

                $getSession->docs = array_merge(data_get($getSession, "docs", []), ["main" => $mainDoc->type]);
                $getSession->save();
                $session_id = data_get($getSession, "session");
                return response()->json($this->responseHelper->successWithData(["doc" => $mainDoc->type."-front", "document" => $document."-front", "session" => $session_id]), 200);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step7Back(Request $request)
    {
        $userId = auth()->user()->id;
        $input = $request->validate([
            "back" => "required_if:type,nid,dvl|file|mimetypes:image/jpg,image/png,image/jpeg|max:5200",
        ], 
        [
            "back.required" => __("File is required."), 
            "back.max" => __("File exceeds the maximum upload file size of 5 MB."), 
            "back.file" => __("Please select a valid file type and try once again."), 
            "back.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), 
            "back.uploaded" => __("Unable to upload file due to technical issues."),
        ]);
        $getSession = KycSessions::started()->where("user_id", $userId)->first();

        $basicInfo = $this->getBasicInfo();
        if (blank($getSession)) {
            $addSession = $this->addNewSession($basicInfo);
            $getSession = $addSession;
        }

        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                $hash = cipher(now()->timestamp);
                $mainDoc = $getSession->document("main");
                $document = short_to_docs($mainDoc->type);
                $file = $request->back;
                $exten = strtolower($file->getClientOriginalExtension());
                $name = auth()->id() . "_" . $mainDoc->type . "_" . $hash . "." . $exten;
                $file->storeAs($this->pathKyc, $name);
                $resultFile = ["back" => $name];

                $kycDocs = new \Modules\BasicKYC\Models\KycDocs();
                $kycDocs->session_id = $getSession->id;
                $kycDocs->user_id = $getSession->user_id;
                $kycDocs->type = $mainDoc->type;
                $kycDocs->meta = $mainDoc->meta;
                $kycDocs->files = $resultFile;
                $kycDocs->save();
                // $mainDoc->files = array_merge($mainDoc->files, $resultFile);
                // $mainDoc->files = $resultFile;
                // $mainDoc->save();

                $getSession->docs = array_merge(data_get($getSession, "docs", []), ["main" => $mainDoc->type]);
                $getSession->save();
                $session_id = data_get($getSession, "session");
                return response()->json($this->responseHelper->successWithData(["doc" => $mainDoc->type."-back", "document" => $document."-back", "session" => $session_id]), 200);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step7Selfie(Request $request)
    {
        $userId = auth()->user()->id;
        $input = $request->validate([
            "proof" => "required_if:type,nid,dvl|file|mimetypes:image/jpg,image/png,image/jpeg|max:5200",
        ], 
        [
            "proof.required" => __("File is required."), 
            "proof.max" => __("File exceeds the maximum upload file size of 5 MB."), 
            "proof.file" => __("Please select a valid file type and try once again."), 
            "proof.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), 
            "proof.uploaded" => __("Unable to upload file due to technical issues."),
        ]);
        $getSession = KycSessions::started()->where("user_id", $userId)->first();

        $basicInfo = $this->getBasicInfo();
        if (blank($getSession)) {
            $addSession = $this->addNewSession($basicInfo);
            $getSession = $addSession;
        }

        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                $hash = cipher(now()->timestamp);
                $mainDoc = $getSession->document("main");
                $document = short_to_docs($mainDoc->type);
                $file = $request->proof;
                $exten = strtolower($file->getClientOriginalExtension());
                $name = auth()->id() . "_" . $mainDoc->type . "_" . $hash . "." . $exten;
                $file->storeAs($this->pathKyc, $name);
                $resultFile = ["proof" => $name];

                $kycDocs = new \Modules\BasicKYC\Models\KycDocs();
                $kycDocs->session_id = $getSession->id;
                $kycDocs->user_id = $getSession->user_id;
                $kycDocs->type = $mainDoc->type;
                $kycDocs->meta = $mainDoc->meta;
                $kycDocs->files = $resultFile;
                $kycDocs->save();
                // $mainDoc->files = array_merge($mainDoc->files, $resultFile);
                // $mainDoc->files = $resultFile;
                // $mainDoc->save();

                $getSession->docs = array_merge(data_get($getSession, "docs", []), ["main" => $mainDoc->type]);
                $getSession->save();
                $session_id = data_get($getSession, "session");
                return response()->json($this->responseHelper->successWithData(["doc" => $mainDoc->type."-proof", "document" => $document."-proof", "session" => $session_id]), 200);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step8(Request $request)
    {
        $rules = [];
        $messages = [];
        $rules["twitter"] = "nullable|string|max:100";
        $rules["facebook"] = "nullable|string|max:100";
        $rules["instagram"] = "nullable|string|max:100";
        $rules["tiktok"] = "nullable|string|max:100";
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["twitter","facebook","instagram","tiktok"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step9(Request $request)
    {
        $rules = [];
        $messages = [];
        $rules["education"] = "required|string|max:100";
        $messages["education.required"] = __("Please Choose your Education.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["education"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step10(Request $request)
    {
        $rules = [];
        $messages = [];
        $rules["marital_status"] = "required|string|max:100";
        $messages["marital_status.required"] = __("Please Choose your Marital Status.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["marital_status"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step11(Request $request)
    {
        $rules = [];
        $messages = [];
        $rules["children"] = "required|string|max:100";
        $messages["children.required"] = __("Please Choose your Marital Status.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["children"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function step12(Request $request)
    {
        $rules = [];
        $messages = [];
        $rules["monthly_income"] = "required|string|max:100";
        $rules["source_income"] = "required|string|max:100";
        $rules["monthly_liabilities"] = "nullable|string|max:100";
        $messages["monthly_income.required"] = __("Please Input your Monthly Income.");
        $messages["source_income.required"] = __("Please Choose your Source Income.");
        // $messages["monthly_liabilities.required"] = __("Please Choose your Monthly Liability.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["monthly_income","source_income","monthly_liabilities"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithData($updateData), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function basicAddressUpdate(Request $request)
    {
        $rules = [];
        $messages = [];
        $require = data_get(gss("kyc_fields"), "profile.address.req") == "yes" ? true : false;
        $rules["address_line_1"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $rules["address_line_2"] = "nullable";
        $rules["city"] = $require ? "required|string|max:50" : "nullable|string|max:100";
        $rules["state"] = "nullable|string|max:50";
        $rules["zip"] = "nullable|string|max:20";
        $rules["country"] = "required|string|max:50";
        $messages["address_line_1.required"] = __("Please enter your valid address.");
        $messages["city.required"] = __("Please enter your city name.");
        $messages["country.required"] = __("Please select your country name.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["address_line_1", "address_line_2", "city", "state", "zip", "country"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return response()->json($this->responseHelper->successWithoutData('Update Address Success'), 200);
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function documentsUpdate(Request $request)
    {
        $fields = data_get(gss("kyc_docs"), "field");
        $rules = ["type" => "required|in:pp,nid,dvl", "country" => "required|string|max:50"];
        $messages = ["type.in" => __("Please select a valid document type."), "type.required" => __("Please select the type of document."), "country.required" => __("Please select the issued by country.")];
        if (data_get($fields, "id.show") == "on") {
            $rules["number"] = data_get($fields, "id.req") == "on" ? "required|string|max:50" : "nullable|string|max:50";
            $messages["number.required"] = __("Please enter your document number.");
        }
        if (data_get($fields, "issue.show") == "on") {
            $rules["issue"] = data_get($fields, "issue.req") == "on" ? "required|date_format:m/d/Y" : "nullable|date_format:m/d/Y";
            $messages["issue.required"] = __("Please enter your document issue date.");
            $messages["issue.date_format"] = __("Enter issue date in this 'mm/dd/yyyy' format.");
        }
        if (data_get($fields, "expiry.show") == "on") {
            $rules["expiry"] = data_get($fields, "expiry.req") == "on" ? "required|date_format:m/d/Y" : "nullable|date_format:m/d/Y";
            $messages["expiry.required"] = __("Please enter your document expiry date.");
            $messages["expiry.date_format"] = __("Enter expiry date in this 'mm/dd/yyyy' format.");
        }
        $validatedData = $request->validate($rules, $messages);
        $docType = data_get($validatedData, "type");
        $docMeta = Arr::only($validatedData, ["country", "number", "issue", "expiry"]);
        $docMeta = array_map("strip_tags_map", $docMeta);
        $userId = auth()->user()->id;
        $input = $request->validate([
            "document" => "required_if:type,pp|file|mimetypes:image/jpg,image/png,image/jpeg|max:5200",
            "front" => "required_if:type,nid,dvl|file|mimetypes:image/jpg,image/png,image/jpeg|max:5200",
            "back" => "required_if:type,nid,dvl|file|mimetypes:image/jpg,image/png,image/jpeg|max:5200"
        ], 
        [
            "document.required" => __("File is required."), 
            "document.max" => __("File exceeds the maximum upload file size of 5 MB."), 
            "document.file" => __("Please select a valid file type and try once again."), 
            "document.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), 
            "document.uploaded" => __("Unable to upload file due to technical issues."),
            "front.required" => __("File is required."), 
            "front.max" => __("File exceeds the maximum upload file size of 5 MB."), 
            "front.file" => __("Please select a valid file type and try once again."), 
            "front.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), 
            "front.uploaded" => __("Unable to upload file due to technical issues."),
            "back.required" => __("File is required."), 
            "back.max" => __("File exceeds the maximum upload file size of 5 MB."), 
            "back.file" => __("Please select a valid file type and try once again."), 
            "back.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), 
            "back.uploaded" => __("Unable to upload file due to technical issues.")
        ]);
        $proof = gss("kyc_doc_selfie", "no") == "yes" ? true : false;
        if($proof){
            $input = $request->validate([
                "selfie_proof" => "required|file|mimetypes:image/jpg,image/png,image/jpeg|max:5200"
            ], 
            [
                "selfie_proof.required" => __("File is required."), 
                "selfie_proof.max" => __("File exceeds the maximum upload file size of 5 MB."), 
                "selfie_proof.file" => __("Please select a valid file type and try once again."), 
                "selfie_proof.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), 
                "selfie_proof.uploaded" => __("Unable to upload file due to technical issues.")
            ]);
        }
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session")) {
            try {
                $hash = cipher(now()->timestamp);
                $mainDoc = $getSession->document("main");
                $mainFiles = data_get($mainDoc, "files", []);
                $document = short_to_docs($docType);
                if ($request->type == 'pp'){
                    $file = $request->document;
                    $exten = strtolower($file->getClientOriginalExtension());
                    $name = auth()->id() . "_" . $docType . "_" . $hash . "." . $exten;
                    $file->storeAs($this->pathKyc, $name);
                    $resultFile = ["main" => $name];
                } else {
                    $file_front = $request->front;
                    $file_back = $request->back;
                    $exten_front = strtolower($file_front->getClientOriginalExtension());
                    $exten_back = strtolower($file_back->getClientOriginalExtension());
                    $front_name = auth()->id() . "_" . $docType . "_" . $hash . "." . $exten_front;
                    $back_name = auth()->id() . "_" . $docType . "_" . $hash . "." . $exten_back;
                    $file_front->storeAs($this->pathKyc, $front_name);
                    $file_front->storeAs($this->pathKyc, $front_name);
                    $resultFile = ["front" => $front_name, "back" => $back_name];
                }
                if($proof){
                    $selfie_proof = $request->selfie_proof;
                    $exten_selfie_proof = strtolower($selfie_proof->getClientOriginalExtension());
                    $selfie_proof_name = auth()->id() . "_" . $docType . "_" . $hash . "." . $exten_selfie_proof;
                    $selfie_proof->storeAs($this->pathKyc, $selfie_proof_name);
                    $resultFile = array_merge($resultFile, ["proof" => $selfie_proof_name]);
                }
                // if (!blank($mainDoc)) {
                //     $mainDoc->type = $docType;
                //     $mainDoc->meta = $docMeta;
                //     $mainDoc->files = $resultFile;
                //     $mainDoc->save();
                // } else {
                // }
                    $kycDocs = new \Modules\BasicKYC\Models\KycDocs();
                    $kycDocs->session_id = $getSession->id;
                    $kycDocs->user_id = $getSession->user_id;
                    $kycDocs->type = $docType;
                    $kycDocs->meta = $docMeta;
                    $kycDocs->files = $resultFile;
                    $kycDocs->save();
                    $mainDoc = $kycDocs->fresh();
                $getSession->docs = array_merge(data_get($getSession, "docs", []), ["main" => $docType]);
                $getSession->save();
                $session_id = data_get($getSession, "session");
                return response()->json($this->responseHelper->successWithData(["doc" => $docType, "document" => $document, "session" => $session_id]), 200);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }

    public function additional()
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession)) {
            if ($this->docsAlter("opt")) {
                $this->fileCleanup($getSession, "additional");
                Session::put("proof_files", []);
                $getBsDoc = $getSession->document("bs");
                $getUbDoc = $getSession->document("ub");
                $required = $this->docsAlter("req");
                $session_id = data_get($getSession, "session");
                return response()->json($this->responseHelper->successWithData(["getBsDoc" => $getBsDoc, "getUbDoc" => $getUbDoc, "bothRequire" => $required, "session" => $session_id]), 200);
            }
            return $this->finalPreview();
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function additionalUpdate(Request $request)
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession)) {
            $docFiles = Session::get("proof_files", []);
            if (empty($docFiles)) {
                return response()->json($this->responseHelper->errorCustom(422, "Please upload all the necessary documents."), 422);
            }
            $bs = $this->docsAlter("bs");
            $ub = $this->docsAlter("ub");
            $both = $this->docsAlter("req");
            $bsdoc = strtolower(short_to_docs("bs"));
            $ubdoc = strtolower(short_to_docs("ub"));
            $rules = $messages = [];
            if ($bs) {
                if ($both) {
                    $rules["bs"] = "required|string|min:8|max:10";
                    $messages["bs.*"] = __("Please upload the :document.", ["document" => __($bsdoc)]);
                } else {
                    $rules["bs"] = "nullable|string|min:8|max:10";
                    $messages["bs.*"] = __("Please upload the :document.", ["document" => __($bsdoc)]);
                }
            }
            if ($ub) {
                if ($both) {
                    $rules["ub"] = "required|string|min:8|max:10";
                    $messages["ub.*"] = __("Please upload the :document.", ["document" => __($ubdoc)]);
                } else {
                    $rules["ub"] = "nullable|string|min:8|max:10";
                    $messages["ub.*"] = __("Please upload the :document.", ["document" => __($ubdoc)]);
                }
            }
            $input = $request->validate($rules, $messages);
            $docs = array_filter(Arr::only($input, ["bs", "ub"]));
            $bsFile = data_get($docFiles, "bs." . Arr::get($docs, "bs", "0"));
            $ubFile = data_get($docFiles, "ub." . Arr::get($docs, "ub", "0"));
            if ($both) {
                if (!$bsFile && !$ubFile) {
                    return response()->json($this->responseHelper->errorCustom(422, "Upload both bank statement or utility bill."), 422);
                }
                if (!$bsFile) {
                    return response()->json($this->responseHelper->errorCustom(422, "Please upload the :document.", ["document" => __($bsdoc)]), 422);
                }
                if (!$ubFile) {
                    return response()->json($this->responseHelper->errorCustom(422, "Please upload the :document.", ["document" => __($ubdoc)]), 422);
                }
            }
            if ($bs && $ub && !$both && !$bsFile && !$ubFile) {
                return response()->json($this->responseHelper->errorCustom(422, "Upload either bank statement or utility bill."), 422);
            }
            if ($bs && !$ub && !$bsFile) {
                return response()->json($this->responseHelper->errorCustom(422, "Please upload the :document.", ["document" => __($bsdoc)]), 422);
            }
            if ($ub && !$bs && !$ubFile) {
                return response()->json($this->responseHelper->errorCustom(422, "Please upload the :document.", ["document" => __($ubdoc)]), 422);
            }
            try {
                return $this->wrapInTransaction(function ($docs, $docFiles, $getSession) {
                    if (!empty($docs) && is_array($docs)) {
                        $bs_ub = [];
                        $isBs = $isUb = false;
                        foreach ($docs as $key => $hash) {
                            $file = data_get($docFiles, $key . "." . $hash);
                            if (!empty($file) && Storage::exists($this->pathTemp . "/" . $file)) {
                                $proofDoc = $getSession->document($key);
                                $this->addOrUpdateProofDoc($proofDoc, $key, $file, $getSession);
                                if ($key == "bs") {
                                    $isBs = true;
                                }
                                if ($key == "ub") {
                                    $isUb = true;
                                }
                            }
                        }
                        if ($isBs) {
                            $bs_ub[] = "bs";
                        }
                        if ($isUb) {
                            $bs_ub[] = "ub";
                        }
                        if (!empty($bs_ub)) {
                            $getSession->docs = array_merge(data_get($getSession, "docs", []), ["proof" => $bs_ub]);
                            $getSession->save();
                        } else {
                            $getSession->docs = array_merge(data_get($getSession, "docs", []), ["proof" => []]);
                            $getSession->save();
                        }
                    }
                    Session::put("proof_files", []);
                    return $this->finalPreview();
                }, $docs, $docFiles, $getSession);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    private function addOrUpdateProofDoc($proof, $key, $file, $session)
    {
        if (!empty($key) && !empty($file)) {
            if (!blank($proof)) {
                $files = data_get($proof, "files", []);
                $this->removeExistFiles($files);
                $proof->type = $key;
                $proof->files = ["main" => $file];
                $proof->save();
            } else {
                $kycDoc = new \Modules\BasicKYC\Models\KycDocs();
                $kycDoc->session_id = $session->id;
                $kycDoc->user_id = $session->user_id;
                $kycDoc->type = $key;
                $kycDoc->files = ["main" => $file];
                $kycDoc->save();
            }
        }
    }

    public function removeOldFile($file)
    {
        $fullpath = $this->pathTemp . "/" . $file;
        if (!empty($file) && Storage::exists($fullpath)) {
            Storage::delete($fullpath);
        }
    }
    private function removeExistFiles($files)
    {
        while (!empty($files)) {
            try {
                foreach ($files as $key => $file) {
                    $this->removeOldFile($file);
                }
            } catch (\Exception $e) {
            }
        }
    }
    private function moveFiles($files)
    {
        if (!empty($files)) {
            foreach ($files as $key => $file) {
                $tempPath = $this->pathTemp . "/" . $file;
                $savePath = $this->pathSave . "/" . $file;
                if (Storage::exists($tempPath)) {
                    if (Storage::exists($savePath)) {
                        Storage::delete($savePath);
                    }
                    Storage::copy($tempPath, $savePath);
                }
            }
        }
    }
    private function moveSubmittedFiles($session)
    {
        if (!blank($session)) {
            $mainDoc = $session->document("main");
            $this->moveFiles(data_get($mainDoc, "files", []));
            $proof = data_get($session, "docs.proof", []);
            if (!empty($proof)) {
                $bsDoc = $session->document("bs");
                if (!blank($bsDoc)) {
                    $bsFiles = data_get($bsDoc, "files", []);
                    if (in_array("bs", $proof)) {
                        $this->moveFiles($bsFiles);
                    } else {
                        $this->removeExistFiles($bsFiles);
                        $bsDoc->files = [];
                        $bsDoc->state = KycDocStatus::INVALID;
                        $bsDoc->save();
                    }
                }
                $ubDoc = $session->document("ub");
                if (!blank($ubDoc)) {
                    $ubFiles = data_get($ubDoc, "files", []);
                    if (in_array("ub", $proof)) {
                        $this->moveFiles($ubFiles);
                    } else {
                        $this->removeExistFiles($ubFiles);
                        $ubDoc->files = [];
                        $ubDoc->state = KycDocStatus::INVALID;
                        $ubDoc->save();
                    }
                }
            }
        }
    }
    private function removeSubmittedFiles($session)
    {
        while (!blank($session)) {
            try {
                $this->wrapInTransaction(function ($session) {
                    $session->docs = [];
                    $session->save();
                    $mainDoc = $session->document("main");
                    $mainFiles = data_get($mainDoc, "files", []);
                    if (!blank($mainDoc)) {
                        $this->removeExistFiles($mainFiles);
                        $mainDoc->files = [];
                        $mainDoc->meta = [];
                        $mainDoc->save();
                    }
                    $bsDoc = $session->document("bs");
                    $bsFiles = data_get($bsDoc, "files", []);
                    if (!blank($bsDoc)) {
                        $this->removeExistFiles($bsFiles);
                        $bsDoc->files = [];
                        $bsDoc->save();
                    }
                    $ubDoc = $session->document("ub");
                    $ubFiles = data_get($ubDoc, "files", []);
                    if (!blank($ubDoc)) {
                        $this->removeExistFiles($ubFiles);
                        $ubDoc->files = [];
                        $ubDoc->save();
                    }
                }, $session);
            } catch (\Exception $e) {
            }
        }
    }
    private function docsAlter($key = NULL)
    {
        $altdoc = data_get(gss("kyc_docs"), "alter");
        $options = ["opt" => false, "req" => false, "bs" => false, "ub" => false, "check" => false];
        if (!empty($altdoc) && is_array($altdoc)) {
            $ub = array_key_exists("ub", $altdoc);
            $bs = array_key_exists("bs", $altdoc);
            $req = array_key_exists("required", $altdoc) && $ub && $bs;
            $options = ["opt" => $bs || $ub ? true : false, "req" => $req, "bs" => $bs, "ub" => $ub];
        }
        if (!empty($key)) {
            return isset($options[$key]) ? $options[$key] : NULL;
        }
        return $options;
    }
    private function fileCleanup($session, $clean = NULL)
    {
        if (!blank($session)) {
            if ($clean == "document") {
                $uploaded = "main_files";
            } else {
                if ($clean == "additional") {
                    $uploaded = "proof_files";
                } else {
                    $uploaded = "uploaded_files";
                }
            }
            $mainFiles = Session::get($uploaded, []);
            if (!empty($mainFiles)) {
                foreach ($mainFiles as $key => $val) {
                    $file = array_values($val);
                    if (!empty($file[0])) {
                        if ($clean == "document") {
                            $mainDocs = $session->document("main");
                            $oldFiles = data_get($mainDocs, "files", []);
                            if (blank($mainDocs) || empty($oldFiles)) {
                                $this->removeOldFile($file[0]);
                            }
                            if (!empty($oldFiles[$key]) && $oldFiles[$key] != $file[0]) {
                                $this->removeOldFile($file[0]);
                            }
                        } else {
                            if ($clean == "additional") {
                                $bsDocs = $session->document("bs");
                                $bsFiles = data_get($bsDocs, "files", []);
                                if (blank($bsDocs) || empty($bsFiles)) {
                                    $this->removeOldFile($file[0]);
                                }
                                if (!empty($bsFiles[$key]) && $bsFiles[$key] != $file[0]) {
                                    $this->removeOldFile($file[0]);
                                }
                                $ubDocs = $session->document("ub");
                                $ubFiles = data_get($ubDocs, "files", []);
                                if (blank($ubDocs) || empty($ubFiles)) {
                                    $this->removeOldFile($file[0]);
                                }
                                if (!empty($ubFiles[$key]) && $ubFiles[$key] != $file[0]) {
                                    $this->removeOldFile($file[0]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    private function hasDocsFiles($document)
    {
        if (blank($document) || !data_get($document, "files", [])) {
            return false;
        }
        return true;
    }
    
    private function addNewApplicant()
    {
        $userId = auth()->user()->id;
        $reference = $this->kycModule->generateReference();
        $kycApplicant = new KycApplicants();
        $kycApplicant->user_id = $userId;
        $kycApplicant->reference = $reference;
        $kycApplicant->status = KycStatus::STARTED;
        $kycApplicant->save();
        return $kycApplicant->fresh();
    }

    private function addNewSession($profile)
    {
        $userId = auth()->user()->id;
        $sessionId = $this->kycModule->generateUniqueSession();
        $newSession = new KycSessions();
        $newSession->user_id = $userId;
        $newSession->session = $sessionId;
        $newSession->profile = $profile;
        $newSession->method = "manual";
        $newSession->status = KycSessionStatus::STARTED;
        $newSession->save();
        return $newSession->fresh();
    }

    public function viewDetail(Request $request)
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::with(["documents"])->where("user_id", $userId)->first();
        if (!$getSession){
            return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
        }
        
        $getDocuments = !blank($getSession->documents) ? $getSession->documents : NULL;
        return response()->json($this->responseHelper->successWithData(["data" => $getSession, "documents" => $getDocuments]), 200);
    }
}

?>
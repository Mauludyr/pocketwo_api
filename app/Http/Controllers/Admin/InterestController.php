<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;

class InterestController extends Controller
{
    public function index()
    {
        $settings = Setting::where('key','like','%'.'interest_rate'.'%')->orderBy('id','desc')->pluck('value')->first();
        $int_rate = $settings ? $settings : 0;
        return view('admin.interest.index',compact('int_rate'));
    }

    public function changeInterest(Request $request)
    {
        $next_inv = Setting::where('key','like','%'.'interest_rate'.'%')->get()->count() + 1;
        $setting = new Setting;
        $setting->key = 'interest_rate_'.$next_inv;
        $setting->value = $request->interest;
        $setting->save();

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserOtp;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;

class SecurityController extends Controller
{
    public function requestOTP(Request $request)
    {
        $pool = '0123456789';
        $generated_otp = substr(str_shuffle(str_repeat($pool, 5)), 0, 6);

        $createOtp = new UserOtp;
        $createOtp->user_id = Auth::user()->id;
        $createOtp->otp = $generated_otp;
        $createOtp->valid_until = Carbon::now()->addMinutes(5);
        $createOtp->save();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://api.twilio.com/2010-04-01/Accounts/AC1d6342d8991fe7b8235b756aa53ab5f3/Messages.json");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('Body' => 'Your Pocketwo OTP is '.$generated_otp, 'From' => '+17083364272', 'To' => $request->phone_number));
        curl_setopt($ch, CURLOPT_USERPWD, 'AC1d6342d8991fe7b8235b756aa53ab5f3' . ":" . '5e08231bcf7f089899c0dddebc9fc795');  

        // In real life you should use something like:
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 
        //          http_build_query(array('postvar1' => 'value1')));

        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);

        // $setting = Setting::all();

        // \Config::set('mail.mailers.smtp.host', $setting->where('key','mail_smtp_host')->first()['value']);
        // \Config::set('mail.mailers.smtp.username', $setting->where('key','mail_smtp_user')->first()['value']);
        // \Config::set('mail.mailers.smtp.password', $setting->where('key','mail_smtp_password')->first()['value'] );
        // \Config::set('mail.mailers.smtp.port', $setting->where('key','mail_smtp_port')->first()['value']);
        // \Config::set('mail.mailers.smtp.encryption', $setting->where('key','mail_smtp_secure')->first()['value']);

        // $userEmail = Auth::user()->email;

        // Mail::send(array(), array(), function ($message) use($userEmail,$generated_otp) {
        //     $message->to($userEmail)
        //       ->subject('Pocketwo Security OTP')
        //       ->from('system@pocketwo.com','Pocketwo')
        //       ->setBody('<h1>Security OTP</h1><br/><p>Your security OTP is '.$generated_otp.'. Do not share this OTP code for your account security.</p>', 'text/html');
        // });

        return true;

    }

    public function checkOTP(Request $request)
    {
        $lastValidOTP = UserOtp::where('user_id', Auth::user()->id)->where('valid_until','<', Carbon::now()->addMinutes(5))->orderBy('id','desc')->first();
        if($lastValidOTP->otp != $request->otp)
        {
            return response()->json([
                'message' => 'OTP not valid or expired',
                'error' => true
            ]);
        }
        return response()->json([
            'message' => 'success',
            'error' => false
        ]);
    }
}

<?php


namespace App\Http\Controllers\User;

use App\Enums\TransactionType;
use App\Enums\TransactionStatus;
use App\Enums\PaymentMethodStatus;

use App\Models\Transaction;
use App\Models\Setting;
use App\Models\PaymentMethod;

use App\Services\GraphData;
use App\Services\Transaction\TransactionService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class UserDashboardController extends Controller
{

    public function __construct()
    {
 
    }

    public function index(Request $request, GraphData $graph)
    {

        $transactionService = new TransactionService();

        $interestPayload = [
            'base_currency' => 'USDT',
            'currency' => 'USDT',
            'exchange_rate' => 1,
            'pay_to' => 'TRaUbhsvSy8pUimxZF3pW7eZoFYd6HhtL1'
        ];

        $getInterestData = $transactionService->createInterestTransaction($interestPayload);

        $currentInterestRate = $getInterestData['currentInterestRate'];
        $interestTimeFrame = $getInterestData['interestTimeFrame'];
        $balanceTimeFrame = $getInterestData['balanceTimeFrame'];
        $dateTimeFrame = $getInterestData['dateTimeFrame'];
        $currentBalance = $getInterestData['currentBalance'];
        $currentInterest = $getInterestData['currentInterest'];

        $paymentMethods = PaymentMethod::where('status', PaymentMethodStatus::ACTIVE)
            ->get()->keyBy('slug')->toArray();

        $interestRate = Setting::where('key','like','%'.'interest_rate'.'%')->orderBy('id','desc')->pluck('value')->first();

        $recentTransactions = Transaction::with(['ledger'])
            ->whereIn('status', [TransactionStatus::ONHOLD, TransactionStatus::CONFIRMED, TransactionStatus::COMPLETED])
            ->whereNotIn('type', [TransactionType::REFERRAL])
            ->where('user_id', auth()->user()->id)
            ->orderBy('id', 'desc')
            ->limit(5)->get();

        return view('user.dashboard', compact('paymentMethods','recentTransactions','currentInterestRate','interestTimeFrame','balanceTimeFrame','dateTimeFrame','currentBalance','currentInterest','interestRate'));
    }

}

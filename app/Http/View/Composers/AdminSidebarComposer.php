<?php

namespace App\Http\View\Composers;

use App\Enums\UserRoles;
use App\Enums\TransactionType;
use App\Enums\TransactionStatus;

use App\Models\User;
use App\Models\UserBorrower;
use App\Models\UserLender;
use App\Models\Transaction;
use App\Models\LoanRequest;
use App\Models\LoanOffer;
use App\Models\LoanTransaction;

use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class AdminSidebarComposer
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var Transaction
     */
    private $transaction;

    public function __construct(User $user, Transaction $transaction, UserBorrower $borrower, UserLender $lender, LoanRequest $loanRequest, LoanOffer $loanOffer, LoanTransaction $loanTransaction)
    {
        $this->user = $user;
        $this->borrower = $borrower;
        $this->lender = $lender;
        $this->transaction = $transaction;
        $this->loanrequest = $loanRequest;
        $this->loanoffer = $loanOffer;
        $this->loantransaction = $loanTransaction;
    }

    public function compose(View $view)
    {
        $loanOfferCount = $this->loanoffer->count();
        $loanRequestCount = $this->loanrequest->count();
        $loanTransactionCount = $this->loantransaction->count();

        $userCountByStatus = $this->user->select('status', DB::raw('count(*) as total'))
            ->whereNotIn('role', [UserRoles::ADMIN, UserRoles::SUPER_ADMIN])
            ->groupBy('status')
            ->get()->pluck('total', 'status');

        $adminUserCount = $this->user->whereIn('role', [UserRoles::ADMIN, UserRoles::SUPER_ADMIN])->count();
        $borrowerUserCount = $this->borrower->count();
        $lenderUserCount = $this->lender->count();

        $pendingTransactions = $this->transaction->where('status', TransactionStatus::PENDING)->get();
        $onholdTransactions = $this->transaction->where('status', TransactionStatus::ONHOLD)->get();
        $confirmedTransactions = $this->transaction->where('status', TransactionStatus::CONFIRMED)->get();
        $pendingDepositCount = $pendingTransactions->where('type', TransactionType::DEPOSIT)->count();
        $pendingWithdrawCount = $pendingTransactions->where('type', TransactionType::WITHDRAW)->count();
        $pendingReferralCount = $pendingTransactions->where('type', TransactionType::REFERRAL)->count();

        $view->with([
            'userCount' => $userCountByStatus,
            'LoanRequestCount' => $loanRequestCount,
            'LoanOfferCount' => $loanOfferCount,
            'LoanTransactionCount' => $loanTransactionCount,
            'adminUserCount' => $adminUserCount,
            'borrowerUserCount' => $borrowerUserCount,
            'lenderUserCount' => $lenderUserCount,
            'pendingTransactionCount' => $pendingTransactions->whereNotIn('type', [TransactionType::REFERRAL])->count(),
            'onholdTransactionCount' => $onholdTransactions->count(),
            'confirmedTransactionCount' => $confirmedTransactions->count(),
            'pendingDepositCount' => $pendingDepositCount,
            'pendingWithdrawCount' => $pendingWithdrawCount,
            'pendingReferralCount' => $pendingReferralCount
        ]);
    }
}

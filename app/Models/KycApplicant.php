<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KycApplicant extends Model
{
    protected $table = 'kyc_applicants';

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'reference',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}

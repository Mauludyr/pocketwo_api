<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KycDoc extends Model
{
    protected $table = 'kyc_docs';

    /**
     * @var string[]
     */
    protected $fillable = [
        'session_id',
        'user_id',
        'type',
        'files',
        'meta',
        'state',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function sessions()
    {
        return $this->belongsTo(KycSession::class, 'session_id');
    }

}

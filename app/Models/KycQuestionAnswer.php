<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KycQuestionAnswer extends Model
{
    use HasFactory;

    public function userQuestion(){
        return $this->hasOne(UserKycQuestion::class,'question_answer_id', 'id');
    }
}

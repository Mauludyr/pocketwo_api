<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LenderType extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ["name"];

    public function userLender()
    {
        return $this->hasOne(UserLender::class, 'type_id', 'id');
    }
}

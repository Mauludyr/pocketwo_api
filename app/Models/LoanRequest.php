<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanRequest extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [];

    protected $casts = ["profile" => "array", "documents" => "array"];

    protected $appends = ['loan_score'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function offer(){
        return $this->hasOne(LoanOffer::class,'loan_id', 'id');
    }

    public function transactions(){
        return $this->hasMany(LoanTransaction::class,'loan_id', 'id');
    }
    
    public function getLoanScoreAttribute()
    {
        //Loan Application Calculate
        if ($this->documents){

            $bank_balance = (int)$this->documents['bank_balance'];
            if ($bank_balance < 1000){
                $bank_balance = 0;
            } else if ($bank_balance >= 1000 || $bank_balance <= 3000){
                $bank_balance = (1 * 10 )/ 100;
            } else if ($bank_balance >= 3001 || $bank_balance <= 5000){
                $bank_balance = (2 * 10) / 100;
            } else if ($bank_balance >= 5001 || $bank_balance <= 10000){
                $bank_balance = (3 * 10) / 100;
            } else if ($bank_balance > 10000){
                $bank_balance = (4 * 10) / 100;
            } else {
                $bank_balance = 0;
            }

            $other_asset = (int)$this->documents['other_liquid_asset'];
            if ($other_asset < 1000){
                $other_asset = 0;
            } else if ($other_asset >= 1000 || $other_asset <= 3000){
                $other_asset = 0;
            } else if ($other_asset >= 3001 || $other_asset <= 5000){
                $other_asset = (1 * 5) / 100;
            } else if ($other_asset >= 5001 || $other_asset <= 10000){
                $other_asset = (2 * 5) / 100;
            } else if ($other_asset > 10000){
                $other_asset = (2 * 5) / 100;
            } else {
                $other_asset = 0;
            }

            $credit_bureau = (int)$this->documents['credit_bureau_queries'];
            if ($credit_bureau < 1000){
                $credit_bureau = 0 ;
            } else if ($credit_bureau >= 1000 || $credit_bureau <= 1200){
                $credit_bureau = 0;
            } else if ($credit_bureau >= 1201 || $credit_bureau <= 1500){
                $credit_bureau = (1 * 20) / 100;
            } else if ($credit_bureau >= 1501 || $credit_bureau <= 1800){
                $credit_bureau = (2 * 20) / 100;
            } else if ($credit_bureau >= 1801 || $credit_bureau <= 2000){
                $credit_bureau = (3 * 20) / 100;
            } else {
                $credit_bureau = 0;
            }

            $social_proof = (int)$this->documents['social_proof'];
            if ($social_proof == '0' || $social_proof == '-'){
                $social_proof = 0 ;
            } else if ($social_proof == 1){
                $social_proof = (1 * 10 )/ 100;
            } else if ($social_proof == 2){
                $social_proof = (2 * 10 )/ 100;
            } else if ($social_proof == 3){
                $social_proof = (3 * 10 )/ 100;
            } else if ($social_proof == 4){
                $social_proof = (4 * 10 )/ 100;
            } else if ($social_proof == 5){
                $social_proof = (5 * 10 )/ 100;
            } else {
                $social_proof = 0;
            }

            $total_loan_score = $bank_balance + $other_asset + $credit_bureau + $social_proof;
        } else {
            $total_loan_score = 0;
        }
        
        return $total_loan_score;
    }
}

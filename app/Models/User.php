<?php

namespace App\Models;

use App\Enums\UserRoles;
use App\Enums\UserStatus;
use App\Enums\TransactionType;
use App\Enums\TransactionStatus;
use App\Filters\Filterable;
use App\Services\SocialAuth;
use Carbon\Carbon;

use Modules\BasicKYC\Models\KycSessions;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Filterable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'role',
        'status',
        'refer',
        '2fa',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $appends = [
        'loan_limit'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'last_login' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * @version 1.0.0
     * @since 1.0
     */
    public function verify_token()
    {
        return $this->hasOne(VerifyToken::class, 'email', 'email')->where('user_id', $this->id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function user_metas()
    {
        return $this->hasMany(UserMeta::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function kyc_applicants()
    {
        return $this->hasMany(KycApplicant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function kyc_docs()
    {
        return $this->hasMany(KycDoc::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function kyc_sessions()
    {
        return $this->hasMany(KycSession::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function refer_codes()
    {
        return $this->hasMany(ReferralCode::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.2.0
     * @since 1.2.0
     */
    public function referrals()
    {
        return $this->hasMany(Referral::class, 'refer_by');
    }

    public function getReferralBonusEarnedAttribute()
    {
        return Transaction::where('type', TransactionType::REFERRAL)
            ->where('user_id', $this->id)
            ->where('status', TransactionStatus::COMPLETED)
            ->sum('amount');
    }

    /**
     * @return mixed
     * @version 1.2.0
     * @since 1.0
     */
    public function meta($key = null, $empty = false)
    {
        $userMetas = $this->user_metas()->pluck('meta_value', 'meta_key');

        if (!empty($key)) {
            if ($key == 'addresses') {
                return $this->addressLines();
            }
            if ($key == 'basics') {
                return $this->basicInfo();
            }
            return (Arr::get($userMetas, $key)) ? Arr::get($userMetas, $key) : $empty;
        }

        return $userMetas->toArray();
    }

    /**
     * @return array
     * @version 1.0.0
     * @since 1.2.0
     */
    public function addressLines()
    {
        return [
            'address_line_1' => $this->meta('profile_address_line_1', null),
            'address_line_2' => $this->meta('profile_address_line_2', null),
            'city' => $this->meta('profile_city', null),
            'state' => $this->meta('profile_state', null),
            'zip' => $this->meta('profile_zip', null),
            'country' => $this->meta('profile_country', null),
        ];
    }

    /**
     * @return array
     * @version 1.0.0
     * @since 1.2.0
     */
    public function basicInfo()
    {
        return [
            'name' => $this->name,
            'phone' => $this->meta('profile_phone', null),
            'dob' => $this->meta('profile_dob', null),
            'gender' => $this->meta('profile_gender', null),
            'nationality' => ($this->meta('profile_nationality') == 'same') ? $this->meta('profile_country', null) : $this->meta('profile_nationality', null)
        ];
    }

    /**
     * @return array|\ArrayAccess|mixed
     * @version 1.0.0
     * @since 1.0
     */
    public function getDisplayNameAttribute()
    {
        $userMetas = $this->user_metas()->pluck('meta_value', 'meta_key');
        return (Arr::get($userMetas, 'profile_display_full_name') == 'on') ? $this->name : Arr::get($userMetas, 'profile_display_name');
    }

    /**
     * @return array|\ArrayAccess|mixed
     * @version 1.0.0
     * @since 1.0
     */
    public function getAvatarBgAttribute()
    {
        $userMetas = $this->user_metas()->pluck('meta_value', 'meta_key');
        return (Arr::get($userMetas, 'profile_avatar_bg')) ? Arr::get($userMetas, 'profile_avatar_bg') : 'primary';
    }

    public function getReferrerAttribute()
    {
        return self::where('id', $this->refer)->first();
    }

    /**
     * @return array|\ArrayAccess|mixed
     * @version 1.0.0
     * @since 1.0
     */
    public function getReferralCodeAttribute()
    {
        $userMetas = $this->refer_codes->pluck('meta_value', 'meta_key');
        return (Arr::get($userMetas, 'profile_avatar_bg')) ? Arr::get($userMetas, 'profile_avatar_bg') : 'primary';
    }

    /**
     * @return boolean
     * @version 1.0.0
     * @since 1.0
     */
    public function getIsVerifiedAttribute()
    {
        return (empty($this->meta('email_verified'))) ? false : true;
    }

    /**
     * @return boolean
     * @version 1.0.0
     * @since 1.0
     */
    public function getHasBasicAttribute()
    {
        return ($this->meta('profile_phone') && $this->meta('profile_dob') && $this->meta('profile_display_name') && $this->meta('profile_country')) ? true : false;
    }

    /**
     * @return bool
     * @version 1.0.0
     * @since 1.1.2
     */
    public function getHasSocialAuthAttribute()
    {
        return app()->make(SocialAuth::class)->authExists($this);
    }

    /**
     * @return string
     * @version 1.0.0
     * @since 1.1.2
     */
    public function getConnectedWithAttribute()
    {
        return app()->make(SocialAuth::class)->authExists($this, true);
    }

    /**
     * @return boolean
     * @version 1.0.0
     * @since 1.1.2
     */
    public function getKycVerifiedAttribute()
    {
        return ($this->meta('kyc_verification') == 'verified') ? true : false;
    }

    /**
     * @return boolean
     * @version 1.0.0
     * @since 1.1.2
     */
    public function getKycPendingAttribute()
    {
        return ($this->meta('kyc_verification') == 'pending') ? true : false;
    }

    /**
     * @return boolean
     * @version 1.0.0
     * @since 1.2.0
     */
    public function getKycRejectedAttribute()
    {
        return ($this->meta('kyc_verification') == 'reject') ? true : false;
    }

    /**
     * @return boolean
     * @version 1.0.0
     * @since 1.2.0
     */
    public function getKycStatusAttribute()
    {
        $status = $this->meta('kyc_verification');
        return ($status) ? $status : 'new';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function withdraw_method_details()
    {
        return $this->hasMany(UserAccount::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class)
            ->where('status', 'completed')
            ->orderBy('completed_at', 'desc')
            ->limit(20);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function miscTnx()
    {
        return $this->hasMany(Transaction::class)
            ->whereIn('status', [
                TransactionStatus::CONFIRMED,
                TransactionStatus::ONHOLD,
                TransactionStatus::PENDING,
                TransactionStatus::CANCELLED,
                TransactionStatus::FAILED
            ])
            ->orderBy('id', 'desc')
            ->limit(20);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function activities()
    {
        return $this->hasMany(UserActivity::class)->orderBy('id', 'desc')->limit(20);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function accounts()
    {
        return $this->hasMany(UserAccount::class);
    }

    /**
     * @return mixed|number
     * @version 1.0.0
     * @since 1.0
     */
    public function balance_locked($fund)
    {
        $funds = 0;

        if ($fund == TransactionType::DEPOSIT) {
            $funds = $this->hasMany(Transaction::class)->where('type', $fund)->whereIn('status', [TransactionStatus::PENDING, TransactionStatus::ONHOLD]);
        }
        if ($fund == TransactionType::WITHDRAW) {
            $funds = $this->hasMany(Transaction::class)->where('type', $fund)->whereIn('status', [TransactionStatus::PENDING, TransactionStatus::CONFIRMED, TransactionStatus::ONHOLD]);
        }

        return (!empty($funds)) ? $funds->sum('amount') : 0;
    }

    /**
     * @return mixed |number
     * @version 1.0.0
     * @since 1.0
     */
    public function tnx_amounts($type_of, $calc = 'total')
    {
        switch ($type_of) {
            case 'bonus':
                $type = TransactionType::BONUS;
                break;
            case 'charge':
                $type = TransactionType::CHARGE;
                break;
            case 'deposit':
                $type = TransactionType::DEPOSIT;
                break;
            case 'withdraw':
                $type = TransactionType::WITHDRAW;
                break;
            default:
                $type = false;
        }

        if ($type) {
            $getTnx = $this->hasMany(Transaction::class)->where('type', $type)->whereIn('status', [TransactionStatus::COMPLETED]);

            if (blank($getTnx)) {
                return 0;
            }

            if ($calc == 'month') {
                return $getTnx->whereBetween('completed_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('amount');
            }

            return $getTnx->sum('amount');
        }

        return 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function balance($name = null)
    {
        $account = (empty($name)) ? AccType('main') : $name;
        if (in_array($name, ['locked_amount'])) {
            if ($name == 'locked_amount') {
                return $this->balance_locked(TransactionType::DEPOSIT) + $this->balance_locked(TransactionType::WITHDRAW);
            }
            return 0;
        } else {
            $balances = $this->balances()->pluck('amount', 'balance');

            if (!empty($account)) {
                return (Arr::get($balances, $account)) ? Arr::get($balances, $account) : 0;
            }
            return $balances->toArray();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @version 1.0.0
     * @since 1.0
     */
    public function balances()
    {
        return $this->hasMany(Account::class);
    }

    public function getShortcutDetailsAttribute()
    {
        $data = [
            __("Full Name") => $this->name,
            __("Email") => $this->email,
            __('User ID') => the_uid($this->id),
        ];
        return collect($data);
    }

    public function scopeWithoutSuperAdmin($query)
    {
        return $query->where('role', '<>', UserRoles::SUPER_ADMIN);
    }

    public function getHasValidReferrerAttribute()
    {
        if (empty($this->referrer)) {
            return false;
        }

        return ($this->referrer->is_verified && $this->referrer->status === UserStatus::ACTIVE) ? true : false;
    }

    public function userBorrower()
    {
        return $this->hasOne(UserBorrower::class, 'email', 'email');
    }

    public function kycSessions()
    {
        return $this->hasOne(KycSessions::class, 'user_id', 'id');
    }

    public function userLender()
    {
        return $this->hasOne(UserLender::class, 'email', 'email');
    }

    public function userQuestion(){
        return $this->hasOne(UserKycQuestion::class,'user_id', 'id');
    }

    public function loanRequest(){
        return $this->hasOne(LoanRequest::class,'user_id', 'id');
    }

    public function loanOffer(){
        return $this->hasOne(LoanOffer::class,'user_id', 'id');
    }

    public function getLoanLimitAttribute()
    {
        $kyc = KycSessions::where('user_id', $this->id)->first();
        if (!$kyc){
            return 0;
        }
        //KYC Calculate
        $gender = $kyc->profile['gender'];
        if ($gender == 'Male'){
            $gender = 0;
        } else if ($gender == 'female'){
            $gender = 0.2;
        } else {
            $gender = 0;
        }

        $age = Carbon::parse($kyc->profile['dob'])->diff(Carbon::now())->y;
        if ($age < 25){
            $age = (0 * 10) / 100 ;
        } else if ($age >= 26 && $age < 35){
            $age = (2 * 10 )/ 100;
        } else if ($age >= 35 && $age < 45){
            $age = (3 * 10) / 100;
        } else if ($age >= 45){
            $age = (3 * 10) / 100;
        } else {
            $age = 0;
        }

        $nationality = $kyc->profile['nationality'];
        $country_of_residence = $kyc->profile['country_of_residence'];
        $year_in_country = $kyc->profile['year_in_country'];
 
        $residency = 0;
        if ($nationality == $country_of_residence){
            $residency = (3 * 10) / 100 ;
        } else {
            if ($year_in_country == 'Foreigner < 1 year'){
                $residency = 0;
            } else if ($year_in_country == 'Foreigner 1 - 3 years'){
                $residency = (1 * 10) / 100;
            } else if ($year_in_country == 'Foreigner > 3 years'){
                $residency = (2 * 10) / 100;
            } else {
                $residency = 0;
            }
        }



        $twitter = $kyc->profile['twitter'];
        if(blank($twitter)){
            $twitter = 0;
        } else {
            $twitter = 1;
        }
        $facebook = $kyc->profile['facebook'];
        if(blank($facebook)){
            $facebook = 0;
        } else {
            $facebook = 1;
        }
        $instagram = $kyc->profile['instagram'];
        if(blank($instagram)){
            $instagram = 0;
        } else {
            $instagram = 1;
        }
        $tiktok = $kyc->profile['tiktok'];
        if(blank($tiktok)){
            $tiktok = 0;
        } else {
            $tiktok = 1;
        }

        $social_media_links = $twitter + $facebook + $instagram + $tiktok;

        if ($social_media_links == 1){
            $social_media_links = (1 * 10) / 100 ;
        } else if ($social_media_links == 2){
            $social_media_links = (2 * 10 )/ 100;
        } else if ($social_media_links >= 3){
            $social_media_links = (3 * 10) / 100;
        } else {
            $social_media_links = 0;
        }

        $highest_education = $kyc->profile['education'];
        if ($highest_education == "Secondary school / diploma (completed)"){
            $highest_education = 0 ;
        } else if ($highest_education == "Secondary school / diploma (ongoing)"){
            $highest_education = (1 * 20 )/ 100;
        } else if ($highest_education == "Bachelor's degree (completed/ongoing)"){
            $highest_education = (3 * 20) / 100;
        } else if ($highest_education == "Post-graduate (completed/ongoing)"){
            $highest_education = (3 * 20) / 100;
        } else {
            $highest_education = 0;
        }

        $marital_status = $kyc->profile['marital_status'];
        if ($marital_status == 'Bachelor'){
            $marital_status = (1 * 10) / 100 ;
        } else if ($marital_status == 'Widow(er)'){
            $marital_status = (2 * 10 )/ 100;
        } else if ($marital_status == 'Married/Engaged'){
            $marital_status = (3 * 10) / 100;
        } else {
            $marital_status = 0;
        }

        $children = $kyc->profile['children'];
        if ($children == 'None'){
            $children = 0 ;
        } else if ($children == '> 12 years'){
            $children = (1 * 10 )/ 100;
        } else if ($children == '< 12 years'){
            $children = (3 * 10 )/ 100;
        } else {
            $children = 0;
        }

        $occupation = $kyc->profile['source_income'];
        if ($occupation == 'Unemployed'){
            $occupation = 0 ;
        } else if ($occupation == 'Student / Full-time National Serviceman'){
            $occupation = (1 * 20 )/ 100;
        } else if ($occupation == 'Part-Time / Gig Economy / Freelancer'){
            $occupation = (2 * 20) / 100;
        } else if ($occupation == 'Own business'){
            $occupation = (2 * 20) / 100;
        } else if ($occupation == 'Full-Time'){
            $occupation = (4 * 20) / 100;
        } else {
            $occupation = 0;
        }

        $total_kyc_score = $age + $gender + $residency + $social_media_links + $highest_education + $marital_status + $children + $occupation;
        
        $loan = LoanRequest::where('user_id', $this->id)->where('status', 'Repaid')->get();

        $total_loan_score = 0;
        
        if($loan->isNotEmpty()){
            foreach ($loan as $key => $value) {
                $total_loan_score += $value->loan_score;
            } 
        }
        
        $total_score = $total_kyc_score + $total_loan_score;


        $credit_score = $total_score * 1000; 

        $multiplier = 0;
        if ($credit_score <= 500){
            $multiplier = 0.25 ;
        } else if ($credit_score >= 501 || $credit_score <= 600){
            $multiplier = 0.50;
        } else if ($credit_score >= 601 || $credit_score <= 700){
            $multiplier = 1;
        } else if ($credit_score >= 701 || $credit_score <= 800){
            $multiplier = 1;
        } else if ($credit_score >= 801 || $credit_score <= 900){
            $multiplier = 1.50;
        } else if ($credit_score <= 901){
            $multiplier = 2.00;
        } else {
            $multiplier = 0;
        }

        $monthly_income = (int)$kyc->profile['monthly_income'];

        if($monthly_income == 0){
            $monthly_income = 1; 
        }else{
            $monthly_income = (int)$kyc->profile['monthly_income'];
        } 

        $loanNotRepaid = LoanRequest::where('user_id', $this->id)->whereIn('status', ['Repaid', 'Pending Offer', 'Offer Received'])->get();

        $loan_amount = 0;

        $credit_limit = 0;

        if($loanNotRepaid->isNotEmpty()){
            foreach ($loanNotRepaid as $key => $value) {
                            
                if ($value->status == 'Repaid') {
                    $credit_limit = $multiplier * $monthly_income;
                // } else if ($value->status == 'Pending Offer' || $value->status == 'Offer Received') {
                //     $credit_limit = ($multiplier * $monthly_income) - $loan_amount;
                } else {
                    $loan_amount += $value->loan_amount;
                    $credit_limit = ($multiplier * $monthly_income) - $loan_amount;
                }
            }
        } else {
            $credit_limit = $multiplier * $monthly_income;
        }

        // $credit_limit = ($multiplier * $monthly_income) - $loan_amount;
        return $credit_limit;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserKycQuestion extends Model
{
    use HasFactory;
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function question()
    {
        return $this->belongsTo(KycQuestion::class, 'question_id', 'id');
    }

    public function answer()
    {
        return $this->belongsTo(KycQuestionAnswer::class, 'question_answer_id', 'id');
    }
}

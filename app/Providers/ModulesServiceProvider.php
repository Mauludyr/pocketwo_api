<?php

namespace App\Providers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Finder\SplFileInfo;

class ModulesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            if (!empty($customModules = available_modules('mod'))) {
                foreach ($customModules as $customModule) {

                    if (file_exists($provider = base_path(implode(DIRECTORY_SEPARATOR, ['modules', $customModule, 'Provider', 'RouteServiceProvider.php'])))) {
                        $this->app->register("\Modules\\$customModule\Provider\RouteServiceProvider");
                    }

                    if (file_exists($filtering = base_path(implode(DIRECTORY_SEPARATOR, ['modules', $customModule, 'RequestFilters'])))) {
                        $fileSystem = new Filesystem();
                        collect($fileSystem->files($filtering))->each(function (SplFileInfo $item) use ($customModule) {
                            $alias = strtolower($customModule . '-' . $item->getBasename('Filter.php'));
                            $file = $item->getBasename('.php');
                            $this->app->make(Router::class)->aliasMiddleware($alias, "Modules\\{$customModule}\\RequestFilters\\{$file}");
                        });
                    }

                    if (file_exists($views = base_path(implode(DIRECTORY_SEPARATOR, ['modules', $customModule, 'Views'])))) {
                        $this->loadViewsFrom($views, $customModule);
                    }

                    if (file_exists($config = base_path(implode(DIRECTORY_SEPARATOR, ['modules', $customModule, 'Config', 'module.php'])))) {
                        $this->mergeConfigFrom($config, 'modules');
                    }

                    if (file_exists($migrations = base_path(implode(DIRECTORY_SEPARATOR, ['modules', $customModule, 'Database', 'migrations'])))) {
                        $this->loadMigrationsFrom($migrations);
                    }

                    if (class_exists($customModuleLoader = "\\Modules\\{$customModule}\\{$customModule}Module")) {
                        $this->app->bind(strtolower($customModule), function () use ($customModuleLoader) {
                            return new $customModuleLoader();
                        });
                    }
                }
            }
        } catch (\Exception $e) {
            if (env('APP_DEBUG', false)) {
                save_error_log($e, 'module-service');
            }
        }
    }
}

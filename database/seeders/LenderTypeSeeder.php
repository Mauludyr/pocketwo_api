<?php

namespace Database\Seeders;

use App\Models\LenderType;
use Illuminate\Database\Seeder;

class LenderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lenderList = [
            "Institutional (regulated)",
            "Retail (peer-to-peer)"
        ];


        foreach ($lenderList as $lender) {
            $findLender = LenderType::where('name', $lender)->first();
            if (blank($findLender)) {
                 $createLender = LenderType::create([
                    "name" => $lender
                ]);
            }
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Occupation;
use Illuminate\Database\Seeder;

class OccupationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $occupationList = [
            "Unemployed",
            "Student/Full-time National Serviceman",
            "Part-time/Gig economy/Freelancer",
            "Full-time"
        ];


        foreach ($occupationList as $occupation) {
            $findOccupation = Occupation::where('name', $occupation)->first();
            if (blank($findOccupation)) {
                 $createOccupation = Occupation::create([
                    "name" => $occupation
                ]);
            }
        }
    }
}

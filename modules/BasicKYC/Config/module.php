<?php

use Modules\BasicKYC\BasicKYCModule;

return [
    BasicKYCModule::SLUG => [
        "name" => __("Basic KYC"), 
        "slug" => BasicKYCModule::SLUG, 
        "icon" => "ni ni-check-circle-cut", 
        "full_icon" => "ni ni-check-circle-cut", 
        "system" => [
            "kind" => "ID Verification", 
            "info" => "Additional Module", 
            "type" => BasicKYCModule::MOD_TYPES, 
            "version" => BasicKYCModule::VERSION, 
            "update" => BasicKYCModule::LAST_UPDATE, 
            "description" => "Users allow verify their identity on the platform.", 
            "addons" => true
        ], 
        "prefix" => env("KYC_PREFIX", "REF"), 
        "fields" => BasicKYCModule::getFields()
    ]
];

?>
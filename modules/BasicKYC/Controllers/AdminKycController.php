<?php

namespace Modules\BasicKYC\Controllers;

use App\Models\User;
use App\Models\UserMeta;
use App\Models\UserKycQuestion;
use App\Models\KycQuestionAnswer;
use App\Models\KycQuestion;
use App\Jobs\ProcessEmail;
use App\Traits\WrapInTransaction;
use App\Http\Controllers\Controller;
use Modules\BasicKYC\BasicKYCModule;
use Modules\BasicKYC\Models\KycDocs;
use Modules\BasicKYC\Models\KycSessions;
use Modules\BasicKYC\Models\KycApplicants;

use Modules\BasicKYC\Helpers\KycFilter;
use Modules\BasicKYC\Helpers\KycStatus;
use Modules\BasicKYC\Helpers\KycDocStatus;
use Modules\BasicKYC\Helpers\KycSessionStatus;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class AdminKycController extends Controller
{
    private $module = NULL;
    private $pathTemp = "kyc-temp";
    private $pathSave = "kyc";
    private $pathSection = "kyc-section";
    public function __construct(BasicKYCModule $module)
    {
        $this->module = $module;
    }
    public function index(Request $request, $state = NULL)
    {
        if (is_null($state) || $state == "process") {
            $whereIn = [KycSessionStatus::COMPLETED];
            $listing = "process";
        } else {
            if ($state == "all") {
                $whereIn = [KycSessionStatus::COMPLETED, KycSessionStatus::APPROVED, KycSessionStatus::REJECTED];
                $listing = "all";
            } else {
                $whereIn = $state == "new" ? [KycSessionStatus::STARTED] : [$state];
                $listing = $state;
            }
        }
        $filter = new KycFilter($request);
        $totalCount = KycSessions::whereNotIn("status", [KycSessionStatus::STARTED])->count();
        $checkUserID = []; 
        $user = User::where('role', 'user')->get();
        foreach ($user as $key => $value) {
            array_push($checkUserID, $value->id);
        }
        $sessionQuery = KycSessions::whereIn('user_id', $checkUserID)->whereIn("kyc_sessions.status", $whereIn)->orderBy("kyc_sessions.id", user_meta("kyc_order", "desc"));
        $applicants = $sessionQuery->filter($filter)->paginate(user_meta("kyc_perpage", "10"));
        return view("BasicKYC::admin.list", ["applicants" => $applicants, "totalCount" => $totalCount, "listing" => $listing]);
    }
    public function viewSubmission(Request $request, $session)
    {
        try {
            $getSession = KycSessions::with(["documents"])->where("session", $session)->firstOrFail();
            $user = User::findOrFail($getSession->user_id);
            $getDocuments = !blank($getSession->documents) ? $getSession->documents : NULL;
            $this->deleteDuplicateFiles($getSession);
            $question = UserKycQuestion::with(['user','question','answer'])->where('user_id', $getSession->user_id)->get();
            $score = UserKycQuestion::where('user_id', $getSession->user_id)->sum('score');
            return view("BasicKYC::admin.details", ["data" => $getSession, "documents" => $getDocuments, "question" => $question, "score" => $score, "user" => $user]);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($e->getMessage());
        }
    }
    public function updateStatus(Request $request, $session)
    {
        $action = $request->action;
        $getSession = KycSessions::find($session);
        if (empty($action) || !in_array($action, ["reject", "resubmit", "verify"]) || blank($getSession)) {
            throw ValidationException::withMessages(["error" => __("Something went wrong. Please reload the page and try again.")]);
        }
        if (!blank($getSession) && data_get($getSession, "status") == KycSessionStatus::COMPLETED) {
            $applicant = KycApplicants::where("user_id", $getSession->user_id)->first();
            if (blank($applicant) || data_get($applicant, "status") == KycStatus::STARTED) {
                throw ValidationException::withMessages(["error" => __("Sorry, we are unable to proceed your request.")]);
            }
            if (in_array($applicant->status, [KycStatus::VERIFIED, KycStatus::REJECTED])) {
                throw ValidationException::withMessages(["warning" => __("The documents of applicant is already :status.", ["status" => __($applicant->status)])]);
            }
            if ($applicant->status == KycStatus::RESUBMIT) {
                throw ValidationException::withMessages(["warning" => __("The documents of applicant is already rejected & waiting for new submission.")]);
            }
            try {
                return $this->wrapInTransaction(function ($action, $getSession, $applicant) {
                    $checkedBy = ["id" => auth()->user()->id, "name" => auth()->user()->name];
                    $kycMeta = $action == "verify" ? "verified" : $action;
                    UserMeta::updateOrCreate(["user_id" => $getSession->user_id, "meta_key" => "kyc_verification"], ["meta_value" => $kycMeta]);
                    if ($action == "verify") {
                        $user = User::findOrFail($getSession->user_id);
                        $this->updateProfileInfo($getSession->user_id, $getSession);
                        UserMeta::updateOrCreate(["user_id" => $getSession->user_id, "meta_key" => "kyc_verified_at"], ["meta_value" => Carbon::now()]);
                        KycDocs::where("session_id", $getSession->id)->update(["state" => KycDocStatus::VALID]);
                        $getSession->status = KycSessionStatus::APPROVED;
                        $getSession->checked_by = $checkedBy;
                        $getSession->checked_at = Carbon::now();
                        $getSession->save();
                        $applicant->status = KycStatus::VERIFIED;
                        $applicant->save();
                        ProcessEmail::dispatch('kyc-approved-user', $user);
                        return response()->json(["type" => "success", "reload" => true, "title" => __("Verified the Applicant"), "msg" => __("The documents of applicant has been successfully verified.")]);
                    }
                    if ($action == "resubmit" || $action == "reject") {
                        $user = User::findOrFail($getSession->user_id);
                        KycDocs::where("session_id", $getSession->id)->update(["state" => KycDocStatus::INVALID]);
                        $getSession->status = KycSessionStatus::REJECTED;
                        $getSession->checked_by = $checkedBy;
                        $getSession->checked_at = Carbon::now();
                        $getSession->save();
                        $kycStatus = $action == "reject" ? KycStatus::REJECTED : KycStatus::RESUBMIT;
                        $applicant->status = $kycStatus;
                        $applicant->save();
                        $title = $action == "reject" ? __("Rejected the Submission") : __("Requested for Resubmission");
                        $message = $action == "reject" ? __("The documents has been rejected permanently.") : __("The documents has been updated and requested for submit.");
                        if ($applicant->status == 'rejected') {
                            ProcessEmail::dispatch('kyc-rejected-user', $user);
                        } else if ($applicant->status == 'resubmit'){
                            ProcessEmail::dispatch('kyc-resubmission-user', $user);
                        }
                        return response()->json(["type" => "success", "reload" => true, "title" => $title, "msg" => $message]);
                    }
                }, $action, $getSession, $applicant);
            } catch (\Exception $e) {
                save_error_log($e);
                throw ValidationException::withMessages(["error" => __("Sorry, we are unable to proceed your request.")]);
            }
        }
    }
    public function finishStatus(Request $request)
    {
        $applicant = KycApplicants::where("user_id", auth()->user()->id)->first();
        try {
            $applicant->status = KycStatus::FINISHED;
            $applicant->save();
             return response()->json("success");
        } catch (\Exception $e) {
           save_error_log($e);
            throw ValidationException::withMessages(["error" => __("Sorry, we are unable to proceed your request.")]); 
        }
    }
    public function downloadDocFile($part, Request $request)
    {
        $id = $request->get("file", 0);
        if ($id) {
            $docs = KycDocs::find(get_hash($id));
            if (!blank($docs)) {
                $docFile = data_get($docs, "files." . $part);
                $docType = data_get($docs, "type");
                $docName = str_replace($part, $docType . "_" . $part, $docFile);
                if (!empty($docFile)) {
                    // $pathFile = $this->pathSave . "/" . $docFile;
                    $pathFile = $this->pathSection . "/" . $docFile;
                    if (Storage::exists($pathFile)) {
                        return Storage::download($pathFile, $docName);
                    }
                }
            }
        }
        return abort(404);
    }
    public function downloadDocFile2($docFile, Request $request)
    {
        if (!empty($docFile)) {
            $pathFile = $this->pathSection . "/" . $docFile;
            if (Storage::exists($pathFile)) {
                return Storage::download($pathFile, $docFile);
            } else {
                return Storage::download($this->pathSave . "/" . $docFile, $docFile);
            }
        }
        return abort(404);
    }
    public function removeDocFiles($files)
    {
        if (!empty($files)) {
            foreach ($files as $key => $file) {
                $tempPath = $this->pathTemp . "/" . $file;
                if (Storage::exists($tempPath)) {
                    Storage::delete($tempPath);
                }
            }
        }
    }
    private function deleteDuplicateFiles(KycSessions $session)
    {
        try {
            if ($session->status == KycSessionStatus::COMPLETED) {
                $documents = $session->documents()->get();
                if (!blank($documents)) {
                    foreach ($documents as $document) {
                        $files = data_get($document, "files", []);
                        $this->removeDocFiles($files);
                    }
                }
            }
        } catch (\Exception $e) {
        }
    }
    private function updateProfileInfo($userId, $session)
    {
        $user = User::findOrFail($userId);
        $basic = array_merge($user->meta("basics"), $user->meta("addresses"));
        $profile = $session->profile;
        if (!blank($user)) {
            $nameUpdate = false;
            if (data_get($profile, "name") && $user->name != data_get($profile, "name")) {
                $nameUpdate = true;
                $user->name = strip_tags(data_get($profile, "name"));
                $user->save();
            }
            $metaData = \Illuminate\Support\Arr::except($profile, "name");
            $metaUpdate = false;
            foreach ($metaData as $key => $value) {
                $value = strip_tags($value);
                if (!empty($value) && $value != $user->meta("profile_" . $key)) {
                    UserMeta::updateOrCreate(["user_id" => $user->id, "meta_key" => "profile_" . $key], ["meta_value" => $value]);
                    $metaUpdate = true;
                }
            }
            if ($nameUpdate || $metaUpdate) {
                $session->revised = $basic;
                $session->save();
            }
        }
    }
}

?>
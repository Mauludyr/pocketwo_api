<?php

namespace Modules\BasicKYC\Controllers;

use Modules\BasicKYC\BasicKYCModule;
use Modules\BasicKYC\Helpers\ImportSettings;

use App\Models\Setting;
use App\Http\Controllers\Controller;
use App\Traits\WrapInTransaction;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class KycSettingsController extends Controller
{
    private $module = NULL;
    private $pathTemp = "kyc-temp";
    private $pathSave = "kyc";
    public function __construct(BasicKYCModule $module)
    {
        $this->module = $module;
    }
    public function index()
    {
        $getFields = BasicKYCModule::getFields();
        $mainDocs = data_get($getFields, "docs.main");
        $additionalDocs = data_get($getFields, "docs.additional");
        $profileFields = data_get($getFields, "profile");
        $socialFields = data_get($getFields, "social_media_links");
        $compatibility = $this->module->compatible();
        $outdated = $this->module->compatible() ? false : $this->module->minAppVer();
        if ($compatibility) {
            $imported = new ImportSettings();
            $version = $imported->vers();
            $updated = hss("mod_kyc_update", NULL);
            $imported->initialize();
            upss("mod_kyc_update", $version);
        }
        return view("BasicKYC::admin.settings", compact("profileFields", "mainDocs", "additionalDocs", "compatibility", "outdated", "socialFields"));
    }
    private function validateSettings(Request $request)
    {
        if (filled($formType = $request->get("form_type"))) {
            $method = "validate" . Str::studly($formType);
            if (method_exists($this, $method)) {
                return $this->{$method}($request);
            }
        }
        throw ValidationException::withMessages([__("Unable to performed this action!")]);
    }
    private function validateKycSettings($request)
    {
        $input = $request->validate(["feature_enable" => "required|in:yes,no", "verification" => "required|in:yes,no", "verified" => "nullable|array", "profile_locked" => "required|in:yes,no", "profile_complete" => "required|in:yes,no", "disable_request" => "required|in:yes,no", "disable_title" => "nullable", "disable_notice" => "nullable"]);
        if (empty($input["verified"])) {
            $input["verified"] = NULL;
        }
        return $input;
    }
    private function validateKycFormSettings($request)
    {
        $input = $request->validate(["preview_quick" => "required|in:yes,no", "fields" => "nullable|array", "docs" => "array", "docs.main" => "required|array", "doc_selfie" => "required|in:yes,no","social_proof" => "required|in:yes,no"], ["docs.main.required" => __("Please select at least one document for verification purpose.")]);
        if (empty($input["docs"])) {
            $input["docs"] = NULL;
        }
        return $input;
    }
    public function saveSettings(Request $request)
    {
        try {
            if (!Storage::exists($this->pathTemp)) {
                Storage::makeDirectory($this->pathTemp);
            }
            if (!Storage::exists($this->pathSave)) {
                Storage::makeDirectory($this->pathSave);
            }
            $settings = $this->validateSettings($request);

            return $this->wrapInTransaction(function ($settings) {
                foreach ($settings as $key => $value) {
                    $prefix = "kyc";
                    $key = !empty($prefix) ? $prefix . "_" . $key : $key;
                    $value = is_array($value) ? json_encode($value) : $value;
                    Setting::updateOrCreate(["key" => $key], ["value" => $value]);
                }
                return response()->json(["msg" => __("Basic KYC Settings successfully updated.")]);
            }, $settings);
        } catch (ValidationException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw ValidationException::withMessages(["kyc" => __("Unable to proceed. Please check your storage folder permission.")]);
        }
    }
}

?>
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKycApplicantsTable extends Migration
{
    public function up()
    {
        Schema::create("kyc_applicants", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("user_id")->index();
            $table->string("reference")->unique();
            $table->string("status");
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists("kyc_applicants");
    }
}

?>
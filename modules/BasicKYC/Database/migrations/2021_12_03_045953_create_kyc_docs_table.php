<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKycDocsTable extends Migration
{
    public function up()
    {
        Schema::create("kyc_docs", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("session_id");
            $table->bigInteger("user_id")->index();
            $table->string("type")->nullable();
            $table->longText("files")->nullable();
            $table->longText("meta")->nullable();
            $table->integer("state")->default(1);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists("kyc_docs");
    }
}

?>
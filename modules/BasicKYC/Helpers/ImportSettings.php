<?php

namespace Modules\BasicKYC\Helpers;

use App\Enums\EmailRecipientType;
use App\Enums\EmailTemplateStatus;

use App\Models\Setting;
use App\Models\EmailTemplate;

class ImportSettings
{
    const VERSION = 20211026;
    public function vers()
    {
        return self::VERSION;
    }
    public function initialize()
    {
        $this->addEmailTemplates();
        $this->addNewSettings();
    }
    private function addEmailTemplates()
    {
        $templates = [
            "kyc-submission-user" => [
                "name" => "KYC Submission", 
                "slug" => "kyc-submission-user", 
                "group" => "kyc", 
                "recipient" => EmailRecipientType::CUSTOMER, 
                "status" => EmailTemplateStatus::ACTIVE, 
                "params" => [
                    "regards" => "on"
                ], 
                "subject" => "Identity verification submission successfull.", 
                "greeting" => "Hello [[user_name]],", 
                "content" => "Thank you! You have successfully submitted your documents. \n\nOne of our team will review as soon and confirm you. \n\nFeel free to contact us if you have any questions."
            ], 
            "kyc-submission-admin" => [
                "name" => "KYC Submission", 
                "slug" => "kyc-submission-admin", 
                "group" => "kyc", 
                "recipient" => EmailRecipientType::ADMIN, 
                "status" => EmailTemplateStatus::ACTIVE, 
                "params" => [
                    "regards" => "off"
                ], 
                "subject" => "KYC Submission #[[kyc_id]]", 
                "greeting" => "Hello Admin,", 
                "content" => "You have new KYC submission (#[[kyc_id]]).\nThe details are given below: \n\nKYC ID:[[kyc_id]],\n\n User Details:\n[[user_detail]] \n\nThis is an automatic email confirmation, please check details from dashboard.\n\nThank You.", 
                "shortcut" => "[[user_detail]], [[kyc_id]]"
            ], 
            "kyc-approved-user" => [
                "name" => "KYC Approved", 
                "slug" => "kyc-approved-user", 
                "group" => "kyc", 
                "recipient" => EmailRecipientType::CUSTOMER, 
                "status" => EmailTemplateStatus::ACTIVE, 
                "params" => [
                    "regards" => "on"
                ], 
                "subject" => "Identity Verification Successfull", 
                "greeting" => "Hello [[user_name]]", 
                "content" => "Your identity verification is complete. \n\nThank You.", 
                "shortcut" => "[[user_name]]"
            ], 
            "kyc-rejected-user" => [
                "name" => "KYC Rejected", 
                "slug" => "kyc-rejected-user", 
                "group" => "kyc", 
                "recipient" => EmailRecipientType::CUSTOMER, 
                "status" => EmailTemplateStatus::ACTIVE, 
                "params" => [
                    "regards" => "on"
                ], 
                "subject" => "Identity Verification Rejected", 
                "greeting" => "Hello [[user_name]],", 
                "content" => "Your documents for identity verification is rejected. Rejection Reason:\n[[reason]]\n\nFeel free to contact us if you have any questions.", "shortcut" => "[[user_name]], [[reason]]"
            ], 
            "kyc-resubmission-user" => [
                "name" => "KYC Resubmission", 
                "slug" => "kyc-resubmission-user", 
                "group" => "kyc", 
                "recipient" => EmailRecipientType::CUSTOMER, 
                "status" => EmailTemplateStatus::ACTIVE, 
                "params" => [
                    "regards" => "on"
                ], 
                "subject" => "Resubmission Required for Verification", 
                "greeting" => "Hello [[user_name]],", 
                "content" => "Your documents for identity verification was reviewed by us.\nBut some document needs a resubmission for further review.\n\nDocument details: [[document_name]],\nNote: [[note]].\nPlease resubmit those documents again to proceed.", 
                "shortcut" => "[[document_name]], [[user_name]], [[note]]"
            ]
        ];
        foreach ($templates as $slug => $template) {
            $exist = EmailTemplate::where("slug", $slug)->count();
            if ($exist == 0) {
                 EmailTemplate::create($template);
            }
        }
    }
    private function addNewSettings()
    {
        $settings = [
            "kyc_feature_enable" => "no", 
            "kyc_verification" => "yes", 
            "kyc_verified" => NULL, 
            "kyc_profile_locked" => "yes", 
            "kyc_profile_complete" => "yes", 
            "kyc_disable_request" => "no", 
            "kyc_disable_title" => "Temporarily unavailable!", 
            "kyc_disable_notice" => NULL, 
            "kyc_preview_quick" => "yes", 
            "kyc_fields" => json_encode([
                "profile" => [
                    "name" => [
                        "show" => "yes", 
                        "req" => "yes"
                    ], 
                    "dob" => [
                        "show" => "yes", 
                        "req" => "yes"
                    ], 
                    "nationality" => [
                        "show" => "yes"
                    ], 
                    "country" => [
                        "show" => "yes", 
                        "req" => "yes"
                    ], 
                    "address" => [
                        "show" => "yes"
                    ]
                ]
            ]), 
            "kyc_docs" => json_encode([
                "main" => [
                    "pp" => "on", 
                    "nid" => "on"
                ], 
                "alter" => [
                    "bs" => "on"
                ]
            ]), 
            "kyc_doc_country" => "yes", 
            "kyc_doc_selfie" => "yes",
            "kyc_social_proof" => "yes"
        ];
        foreach ($settings as $key => $value) {
            $exist = Setting::where("key", $key)->count();
            if ($exist == 0) {
                Setting::create(["key" => $key, "value" => $value]);
            }
        }
    }
}

?>
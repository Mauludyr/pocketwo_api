<?php

namespace Modules\BasicKYC\Helpers;

final class KycDocStatus
{
    const NEW = 1;
    const VALID = 2;
    const INVALID = 3;
}

?>
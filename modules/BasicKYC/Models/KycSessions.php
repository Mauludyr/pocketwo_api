<?php

namespace Modules\BasicKYC\Models;

use App\Models\User;
use App\Models\LoanRequest;
use App\Models\UserKycQuestion;
use App\Filters\Filterable;
use Carbon\Carbon;
use Modules\BasicKYC\Models\KycApplicants;
use Modules\BasicKYC\Models\KycDocs;
use Modules\BasicKYC\Helpers\KycSessionStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class KycSessions extends Model
{
    use HasFactory, Filterable;
    
    protected $fillable = [];
    protected $casts = ["docs" => "array", "profile" => "array", "revised" => "array", "checked_by" => "array"];
    protected $appends = [
        'kyc_question',
        'loan_limit'
    ];
    public function scopeStarted($query)
    {
        return $query->where("status", KycSessionStatus::STARTED)->orderBy("id", "desc");
    }
    public function user()
    {
        return $this->belongsTo("App\\Models\\User", "user_id");
    }
    public function kyc()
    {
        return $this->belongsTo(KycApplicants::class, "user_id", "user_id");
    }
    public function documents()
    {
        return $this->hasMany(KycDocs::class, "session_id");
    }
    public function document($type)
    {
        if (!empty($type) && in_array($type, ["bs", "ub"])) {
            return $this->documents->where("type", $type)->sortByDesc("id")->first();
        }
        if (!empty($type) && $type == "proof") {
            return $this->documents->whereIn("type", ["bs", "ub"])->sortByDesc("id")->first();
        }
        return $this->documents->whereIn("type", ["pp", "nid", "dvl"])->sortByDesc("id")->first();
    }
    public function doc_main()
    {
        return $this->document("main");
    }
    public function doc_bs()
    {
        return $this->document("bs");
    }
    public function doc_ub()
    {
        return $this->document("ub");
    }
    public function data($key = NULL)
    {
        if (!empty($key)) {
            return data_get($this->profile, $key);
        }
        return $this->profile;
    }
    public function in_docs($key)
    {
        $docs = $this->docs;
        if (in_array($key, ["pp", "nid", "dvl"])) {
            return data_get($docs, "main") == $key ? true : false;
        }
        if (in_array($key, ["ub", "bs"])) {
            return in_array($key, data_get($docs, "proof")) ? true : false;
        }
        return NULL;
    }
    public function getMainDocTypeAttribute()
    {
        return data_get($this->doc_main(), "type");
    }
    public function getMainDocMetaAttribute()
    {
        return data_get($this->doc_main(), "meta", []);
    }
    public function getMainDocFilesAttribute()
    {
        return data_get($this->doc_main(), "files", []);
    }

    public function getkycquestionAttribute()
    {

        $question = UserKycQuestion::where('user_id', $this->user_id)->get();
        if (!$question){
            return null;
        }

        return $question;
    }

    public function getLoanLimitAttribute()
    {
        $kyc = KycSessions::where('session', $this->session)->first();
        if (!$kyc){
            return 0;
        }
        //KYC Calculate
        $gender = $kyc->profile['gender'];
        if ($gender == 'male'){
            $gender = 0;
        } else if ($gender == 'female'){
            $gender = 0.2;
        } else {
            $gender = 0;
        }

        $age = Carbon::parse($kyc->profile['dob'])->diff(Carbon::now())->y;
        if ($age < 25){
            $age = (0 * 10) / 100 ;
        } else if ($age >= 26 && $age < 35){
            $age = (2 * 10 )/ 100;
        } else if ($age >= 35 && $age < 45){
            $age = (3 * 10) / 100;
        } else if ($age >= 45){
            $age = (3 * 10) / 100;
        } else {
            $age = 0;
        }

        $nationality = $kyc->profile['nationality'];
        $country_of_residence = $kyc->profile['country_of_residence'];
        $year_in_country = $kyc->profile['year_in_country'];
 
        $residency = 0;
        if ($nationality == $country_of_residence){
            $residency = (3 * 10) / 100 ;
        } else {
            if ($year_in_country == 'Foreigner < 1 year'){
                $residency = 0;
            } else if ($year_in_country == 'Foreigner 1 - 3 years'){
                $residency = (1 * 10) / 100;
            } else if ($year_in_country == 'Foreigner > 3 years'){
                $residency = (2 * 10) / 100;
            } else {
                $residency = 0;
            }
        }



        $twitter = $kyc->profile['twitter'];
        if(blank($twitter)){
            $twitter = 0;
        } else {
            $twitter = 1;
        }
        $facebook = $kyc->profile['facebook'];
        if(blank($facebook)){
            $facebook = 0;
        } else {
            $facebook = 1;
        }
        $instagram = $kyc->profile['instagram'];
        if(blank($instagram)){
            $instagram = 0;
        } else {
            $instagram = 1;
        }
        $tiktok = $kyc->profile['tiktok'];
        if(blank($tiktok)){
            $tiktok = 0;
        } else {
            $tiktok = 1;
        }

        $social_media_links = $twitter + $facebook + $instagram + $tiktok;

        if ($social_media_links == 1){
            $social_media_links = (1 * 10) / 100 ;
        } else if ($social_media_links == 2){
            $social_media_links = (2 * 10 )/ 100;
        } else if ($social_media_links >= 3){
            $social_media_links = (3 * 10) / 100;
        } else {
            $social_media_links = 0;
        }

        $highest_education = $kyc->profile['education'];
        if ($highest_education == "Secondary school / diploma (completed)"){
            $highest_education = 0 ;
        } else if ($highest_education == "Secondary school / diploma (ongoing)"){
            $highest_education = (1 * 10 )/ 100;
        } else if ($highest_education == "Bachelor's degree (completed/ongoing)"){
            $highest_education = (3 * 20) / 100;
        } else if ($highest_education == "Post-graduate (completed/ongoing)"){
            $highest_education = (3 * 20) / 100;
        } else {
            $highest_education = 0;
        }

        $marital_status = $kyc->profile['marital_status'];
        if ($marital_status == 'Bachelor'){
            $marital_status = (1 * 10) / 100 ;
        } else if ($marital_status == 'Widow(er)'){
            $marital_status = (2 * 10 )/ 100;
        } else if ($marital_status == 'Married/Engaged'){
            $marital_status = (3 * 10) / 100;
        } else {
            $marital_status = 0;
        }

        $children = $kyc->profile['children'];
        if ($children == 'None'){
            $children = 0 ;
        } else if ($children == '> 12 years'){
            $children = (1 * 10 )/ 100;
        } else if ($children == '< 12 years'){
            $children = (3 * 10 )/ 100;
        } else {
            $children = 0;
        }

        $occupation = $kyc->profile['source_income'];
        if ($occupation == 'Unemployed'){
            $occupation = 0 ;
        } else if ($occupation == 'Student / Full-time National Serviceman'){
            $occupation = (1 * 20 )/ 100;
        } else if ($occupation == 'Part-Time / Gig Economy / Freelancer'){
            $occupation = (2 * 20) / 100;
        } else if ($occupation == 'Own business'){
            $occupation = (2 * 20) / 100;
        } else if ($occupation == 'Full-Time'){
            $occupation = (4 * 20) / 100;
        } else {
            $occupation = 0;
        }

        $total_kyc_score = $age + $gender + $residency + $social_media_links + $highest_education + $marital_status + $children + $occupation;
        
        $loan = LoanRequest::where('user_id', $this->user_id)->where('status', 'Repaid')->get();

        $total_loan_score = 0;

        if($loan->isNotEmpty()){ 
            foreach ($loan as $key => $value) {
                $total_loan_score += $value->loan_score;
            } 
        }

        $total_score = $total_kyc_score + $total_loan_score;


        $credit_score = $total_score / 4.7 * 1000; 

        $multiplier = 0;
        if ($credit_score <= 500){
            $multiplier = 0.25 ;
        } else if ($credit_score > 500 && $credit_score <= 600){
            $multiplier = 0.50;
        } else if ($credit_score > 600 && $credit_score <= 700){
            $multiplier = 1;
        } else if ($credit_score > 700 && $credit_score <= 800){
            $multiplier = 1;
        } else if ($credit_score > 801 && $credit_score <= 900){
            $multiplier = 1.50;
        } else if ($credit_score > 900){
            $multiplier = 2.00;
        } else {
            $multiplier = 0;
        }

        $monthly_income = (int)$kyc->profile['monthly_income'];

        if($monthly_income == 0){
            $monthly_income = 1; 
        }else{
            $monthly_income = (int)$kyc->profile['monthly_income'];
        } 

        $loanNotRepaid = LoanRequest::where('user_id', $this->user_id)->get();

        $loan_amount = 0;
        $credit_limit = 0;
        
        if($loanNotRepaid->isNotEmpty()){
            foreach ($loanNotRepaid as $key => $value) {
                       
                if ($value->status == 'Repaid') {
                    $credit_limit = $multiplier * $monthly_income;
                // } else if ($value->status == 'Pending Offer' || $value->status == 'Offer Received') {
                //     $credit_limit = ($multiplier * $monthly_income) - $loan_amount;
                } else {
                    $loan_amount += $value->loan_amount;     
                    $credit_limit = ($multiplier * $monthly_income) - $loan_amount;
                }
            }
        } else {
            $credit_limit = $multiplier * $monthly_income;
        }

        // $credit_limit = ($multiplier * $monthly_income) - $loan_amount;
        return $credit_limit;
    }
}

?>
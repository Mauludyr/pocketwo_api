<?php

namespace Modules\BasicKYC\RequestFilters;

use Closure;
use App\Helpers\helpers;
use Illuminate\Support\Facades\Auth;
use Modules\BasicKYC\Helpers\KycStatus;
use Modules\BasicKYC\Models\KycApplicants;

class ProgressFilter
{
    public function handle($request, Closure $next)
    {
        $userId = auth()->user()->id;
        $applicant = KycApplicants::where("user_id", $userId)->first();
        if (!blank($applicant) && $applicant->status == KycStatus::VERIFIED) {
            return response()->view("BasicKYC::user.verification-verified");
        }
        if (!blank($applicant) && in_array($applicant->status, [KycStatus::PENDING, KycStatus::REJECTED])) {
            $status = module_msg_of($applicant->status, "status", "BasicKYC");
            if ($request->ajax()) {
                return response()->view("BasicKYC::user.misc.notice", $status);
            }
            return response()->view("BasicKYC::user.verification-status", $status);
        }
        return $next($request);
    }
}

?>
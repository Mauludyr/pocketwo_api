<?php

use Illuminate\Support\Facades\Route;

Route::name("admin.")->middleware('admin')->prefix("admin")->group(function () {
    Route::name("kyc.")->prefix("/kyc")->group(function () {
        Route::get("/applicants/{state?}", "AdminKycController@index")->name("list");
        Route::get("/applicant/view/{session}", "AdminKycController@viewSubmission")->name("view");
        Route::get("/download/{part}", "AdminKycController@downloadDocFile")->name("download");
        Route::get("/document/download/{part}", "AdminKycController@downloadDocFile2")->name("download2");
        Route::post("/submission/{session}/update", "AdminKycController@updateStatus")->name("update");
    });
    Route::name("settings.")->prefix("/settings")->group(function () {
        Route::get("/component/basic-kyc", "KycSettingsController@index")->name("component.kyc");
        Route::post("/component/basic-kyc/update", "KycSettingsController@saveSettings")->name("component.kyc.update");
    });
});
Route::get("identity-verification", "UserKycController@verification")->middleware(['user', 'basickyc-progress', 'basickyc-allow'])->name("user.kyc.verify");
Route::post("/kyc/finish/status", "AdminKycController@finishStatus")->name("finish.status");
Route::name("user.kyc.")->middleware(['user', 'basickyc-progress', 'basickyc-allow'])->prefix('kyc')->group(function () {
    Route::post("api/v2/proceed", "UserKycController@proceedNext")->name("verify.next");
    Route::post("api/v2/uploader", "UserKycController@handleFiles")->name("verify.files");
    Route::post("api/v2/basic", "UserKycController@basicInfo")->name("verify.basic");
    Route::post("api/v2/basic/form", "UserKycController@basicInfoForm")->name("verify.basic.form");
    Route::post("api/v2/basic/update", "UserKycController@basicInfoUpdate")->name("verify.basic.info.update");
    Route::post("api/v2/basic/address", "UserKycController@basicAddressUpdate")->name("verify.basic.address.update");
    Route::post("api/v2/docs", "UserKycController@documents")->name("verify.documents");
    Route::post("api/v2/docs/form", "UserKycController@documentsUpdate")->name("verify.documents.update");
    Route::post("api/v2/docs/upload", "UserKycController@documentsUploadUpdate")->name("verify.documents.upload");
    Route::post("api/v2/proof", "UserKycController@additional")->name("verify.additional");
    Route::post("api/v2/proof/upload", "UserKycController@additionalUpdate")->name("verify.additional.upload");
    Route::post("api/v2/confirm", "UserKycController@submitKyc")->name("submit.application");
    Route::get("/cancel", "UserKycController@cancelKyc")->name("cancel.application");
});

Route::get('/kyc/finished', function () {
    return response()->view("BasicKYC::user.verification-verified");
})->name("kyc.finished");
?>
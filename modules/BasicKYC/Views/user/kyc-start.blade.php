@extends('user.layouts.kyc-master')

@section('title', __('Identity Verification'))

@section('progress-content')
<div class="onboarding-steps-container">
    <span id="kyc-step-1" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 1</h6>
      <div class="text-large">Check Verification</div>
    </span>
    <span id="kyc-step-2" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 2</h6>
      <div class="text-large">Basic Information</div>
    </span>
    <span id="kyc-step-3" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 3</h6>
      <div class="text-large">Identify Documents</div>
    </span>
    <span id="kyc-step-4" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 4</h6>
      <div class="text-large">Proof of Address</div>
    </span>
    <span id="kyc-step-5" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 5</h6>
      <div class="text-large">Submit Verification</div>
    </span>
</div>
@endsection
@section('content')
<div class="nk-content-body">
	<div class="kyc-app wide-xs m-auto" id="kyc-step-container">
		<div class="nk-block wide-xs mx-auto">
			<div class="nk-kyc-app">
				<div class="card card-bordered">
			    <div class="card-inner card-inner-lg">
		        <div class="nk-block-head">
	            <div class="nk-block-head-content">
	                <h4 class="nk-block-title text-center font-weight-bold">{{ __('Welcome onboard!') }}</h4>
	            </div>
		        </div>
		        <div class="nk-content-body">
              <div class="nk-content mt-2">
                  <p class="text-center text-muted">{{ __('A few clicks from creating your personal or admin dashboard.') }}</p>
                  <p class="text-muted text-center">Save your time and money!</p>
                  @if ($resubmit)
                  <a href="{{ route('user.kyc.verify') }}" class="btn btn-lg btn-wider btn-primary">
										<span>{{ __('Resubmit KYC') }}</span>
									</a>
                  @else
                  <a href="{{ route('user.kyc.verify') }}" class="btn btn-lg btn-wider btn-primary">
                    <span>{{ __('Lets Started!') }}</span>
                  </a>
                  @endif
              </div>
		        </div>
		      </div>
		    </div>
        @if ($resubmit)
          <p class="mt-1"><span class="text-soft fs-13px fw-normal">* {{ __("Resubmit all the necessary document to verify your identity.") }}</span></p>
        @endif
	   </div>
		</div>
	</div>
</div>{{-- .nk-content-body --}}
@endsection
@extends('user.layouts.kyc-master')

@section('title', __('Verification Center'))

@section('progress-content')
<div class="onboarding-steps-container">
    <span id="kyc-step-1" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 1</h6>
      <div class="text-large text-white">Check Verification</div>
    </span>
    <span id="kyc-step-2" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 2</h6>
      <div class="text-large text-white">Basic Information</div>
    </span>
    <span id="kyc-step-3" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 3</h6>
      <div class="text-large text-white">Identify Documents</div>
    </span>
    <span id="kyc-step-4" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 4</h6>
      <div class="text-large text-white">Proof of Address</div>
    </span>
    <span id="kyc-step-5" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 5</h6>
      <div class="text-large text-white">Submit Verification</div>
    </span>
    <span id="kyc-step-6" class="onboarding-steps last w-inline-block w--current">
      <h6 class="text-white">Step 6</h6>
      @if($title == 'Identity verification failed!')
          <div class="text-large">Failed Verification</div>
      @else
          <div class="text-large">Waiting Verification</div>
      @endif
    </span>
</div>
@endsection
@section('content')
	<div class="nk-content-body">
        <div class="kyc-app wide-xs m-auto">
            <div class="nk-notice nk-pps-result mt-sm-5">
                <em class="icon icon-circle icon-circle-xxl ni {{ (isset($icon) && $icon) ? $icon : 'ni-alert bg-warning' }}"></em>
                
                @if(isset($title) && $title)
                <h4 class="title">{{ $title }}</h4>
                @endif

                @if(isset($notice) && (data_get($notice, 'caption') || data_get($notice, 'note')))
                <div class="nk-pps-text {{ the_data($notice, 'class', 'md') }}{{ (!$title) ? ' mt-5' : '' }}">
                    @if(data_get($notice, 'caption'))
                    <p class="caption-text">{{ data_get($notice, 'caption') }}</p>
                    @endif
                    @if(data_get($notice, 'note'))
                        <p class="sub-text-sm">{{ data_get($notice, 'note') }}</p>
                    @endif
                </div>
                @endif

                @if((isset($button) && $button) || (isset($link) && $link))
                <div class="nk-pps-action">
                    <ul class="btn-group-vertical align-center gy-3">
                        @if(isset($button) && is_array($button) && data_get($button, 'text') && data_get($button, 'url'))
                        <li><a href="{{ data_get($button, 'url') }}" class="btn btn-lg btn-mw {{ the_data($button, 'class', 'btn-primary') }}">{{ data_get($button, 'text') }}</a></li>
                        @endif
                        @if(isset($link) && is_array($link) && data_get($link, 'text') && data_get($link, 'url'))
                        <li><a href="{{ data_get($link, 'url') }}" class="link {{ the_data($link, 'class', 'link-primary') }}">{{ data_get($link, 'text') }}</a></li>
                        @endif
                    </ul>
                </div>
                @endif

                @if(isset($help) && $help)
                    <div class="nk-pps-notes text-center d-none">
                        {!! $help !!}
                    </div>
                @endif
            </div>  
	    </div>
    </div>
@endsection

@extends('user.layouts.kyc-master')

@section('title', __('Identity Verification'))

@section('progress-content')
<div class="onboarding-steps-container">
    <span id="kyc-step-1" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 1</h6>
      <div class="text-large text-white">Check Verification</div>
    </span>
    <span id="kyc-step-2" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 2</h6>
      <div class="text-large text-white">Basic Information</div>
    </span>
    <span id="kyc-step-3" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 3</h6>
      <div class="text-large text-white">Identify Documents</div>
    </span>
    <span id="kyc-step-4" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 4</h6>
      <div class="text-large text-white">Proof of Address</div>
    </span>
    <span id="kyc-step-5" class="onboarding-steps passed w-inline-block">
      <h6 class="text-white">Step 5</h6>
      <div class="text-large text-white">Submit Verification</div>
    </span>
    <span id="kyc-step-6" class="onboarding-steps passed w-inline-block w--current">
      <h6 class="text-white">Step 6</h6>
      <div class="text-large">Verification Completed</div>
    </span>
</div>
@endsection
@section('content')
	<div class="nk-content-body">
        <div class="kyc-app wide-xs m-auto" id="kyc-step-container">
			<div class="nk-block-head nk-block-head-md wide-xs mx-auto">
				<div class="nk-block-head-content text-center">
					<h2 class="nk-block-title fw-normal">{{ __("Identity Verification") }}</h2>
					<div class="nk-block-des">
						<p>{{ __("To comply with regulation you will have to go through identity verification.") }}</p>
						<p><strong>{{ __("Congrats, we have successfully verified your identity and submitted documents.") }}</strong></p>
					</div>
				</div>
			</div>

			<div class="nk-block wide-xs mx-auto">
				<div class="nk-kyc-app">
					<ul class="list-group">
						<li class="list-group-item">
							<div class="nk-kyc-step d-flex justify-content-between align-items-center py-1">
								<div class="nk-kyc-step-info">
									<h6 class="title mb-1">{{ __("Basic Information") }}</h6>
									<p class="text-soft">{{ __("Your personal information for identity.") }}</p>
								</div>
								<div class="nk-kyc-step-state d-inline-flex align-items-center">
									<span class="icon ni ni-check-circle-fill fs-22px text-success"></span>
								</div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="nk-kyc-step d-flex justify-content-between align-items-center py-1">
								<div class="nk-kyc-step-info">
									<h6 class="title mb-1">{{ __("Identity Documents") }}</h6>
									<p class="text-soft">{{ __("Submit proof of identity document.") }}</p>
								</div>
								<div class="nk-kyc-step-state d-inline-flex align-items-center">
									<span class="icon ni ni-check-circle-fill fs-22px text-success"></span>
								</div>
							</div>
						</li>
						@if (!empty(data_get(gss('kyc_docs'), 'alter')))
						<li class="list-group-item">
							<div class="nk-kyc-step d-flex justify-content-between align-items-center py-1">
								<div class="nk-kyc-step-info">
									<h6 class="title mb-1">{{ __("Proof of Address") }}</h6>
									<p class="text-soft">{{ __("Submit document for address verification.") }}</p>
								</div>
								<div class="nk-kyc-step-state d-inline-flex align-items-center">
									<span class="icon ni ni-check-circle-fill fs-22px text-success"></span>
								</div>
							</div>
						</li>
						@endif
					</ul>
				</div>{{-- .nk-kyc-app --}}
				<div class="nk-kyc-app-note text-center mt-3">
					<div class="note text-soft small">{{ __("Please feel free to contact if you need any further information.") }}</div>
				</div>
				<div class="nk-kyc-app-action mt-5 text-center">
					<a href="{{ route('dashboard') }}" class="btn btn-lg btn-primary"><span>{{ __("Get Started!") }}</span></a>
				</div>
			</div>
	    </div>
    </div>{{-- .nk-content-body --}}
@endsection
@section('custom_js')
<script type="text/javascript">
    $.ajax({
        method:"POST",  
        url:'{{ route('finish.status' ) }}',
        success:function(data){
            console.log(data);
        },
    });
</script>
@endsection
@extends('user.layouts.kyc-master')

@section('title', __('Identity Verification'))

@php

use Modules\BasicKYC\Helpers\KycSessionStatus;

$started = ($status == KycSessionStatus::STARTED) ? true : false;
$iconCheck = 'ni-check-circle-fill fs-22px';
$iconArrow = 'ni-forward-ios fs-14px';
$iconInfo = 'ni-info fs-14px';
$iconAlert = 'ni-alert-c fs-14px';

$pending = __("Submission Pending");
$require = __("Required Resubmission");

@endphp

@section('progress-content')
<div class="onboarding-steps-container">
    <span id="kyc-step-1" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 1</h6>
      <div class="text-large">Check Verification</div>
    </span>
    <span id="kyc-step-2" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 2</h6>
      <div class="text-large">Basic Information</div>
    </span>
    <span id="kyc-step-3" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 3</h6>
      <div class="text-large">Identify Documents</div>
    </span>
    <span id="kyc-step-4" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 4</h6>
      <div class="text-large">Proof of Address</div>
    </span>
    <span id="kyc-step-5" class="onboarding-steps w-inline-block">
      <h6 class="text-white">Step 5</h6>
      <div class="text-large">Submit Verification</div>
    </span>
</div>
@endsection
@section('content')
<div class="nk-content-body">
	<div class="kyc-app wide-xs m-auto" id="kyc-step-container">
		<div class="nk-block-head nk-block-head-md wide-xs mx-auto d-none">
			<div class="nk-pps-steps">
				<span class="step active"></span>
				<span class="step"></span>
				<span class="step"></span>
				@if (!empty(data_get(gss('kyc_docs'), 'alter')))
				<span class="step"></span>
				@endif
				<span class="step"></span>
			</div>
			<div class="nk-block-head-content text-center">
				<h2 class="nk-block-title fw-normal">{{ __("Identity Verification") }}</h2>
				<div class="nk-block-des">
					<p>{{ __("To enable deposits and withdrawals, please verify your identity by submitting the required
						documents.") }}</p>
					<p><strong>{{ __("Complete the verification steps below.") }}</strong></p>
				</div>
			</div>
		</div>

		<div class="nk-block wide-xs mx-auto">
			<div class="nk-kyc-app">
				<ul class="list-group">
					<li class="list-group-item kyc-unext" data-action="basic">
						<div class="nk-kyc-step d-flex justify-content-between align-items-center py-1">
							<div class="nk-kyc-step-info">
								<h6 class="title mb-1">{{ __("Basic Information") }}</h6>
								<p class="text-soft">{{ __("Your personal information for identity.") }}</p>
							</div>
							<div class="nk-kyc-step-state d-inline-flex align-items-center">
								@if ($status == 'none')
								<span class="icon ni {{ $iconArrow }} text-soft"></span>
								@else
								<span class="icon ni {{ (empty($basic)) ? $iconArrow : $iconCheck }} text-soft"></span>
								@endif
							</div>
						</div>
					</li>
					<li class="list-group-item{{ ($started && $basic) ? ' kyc-unext' : '' }}" data-action="docs">
						<div class="nk-kyc-step d-flex justify-content-between align-items-center py-1">
							<div class="nk-kyc-step-info">
								<h6 class="title mb-1">{{ __("Identity Documents") }}</h6>
								<p class="text-soft">{{ __("Submit documents for proof of identity.") }}</p>
							</div>
							<div class="nk-kyc-step-state d-inline-flex align-items-center">
								@if ($document && $basic)
								<span class="icon ni {{ $iconCheck }} text-soft"></span>
								@elseif ($started && $basic)
								<span class="icon ni {{ $iconArrow }} text-soft"></span>
								@else
								<span class="icon ni {{ $resubmit ? $iconAlert : $iconInfo }} text-soft nk-tooltip"
									data-placement="left" title="{{ $resubmit ? $require : $pending }}"></span>
								@endif
							</div>
						</div>
					</li>
					@if (!empty(data_get(gss('kyc_docs'), 'alter')))
					<li class="list-group-item{{ ($started && $basic && $document) ? ' kyc-unext' : '' }}" data-action="proof">
						<div class="nk-kyc-step d-flex justify-content-between align-items-center py-1">
							<div class="nk-kyc-step-info">
								<h6 class="title mb-1">{{ __("Proof of Address") }}</h6>
								<p class="text-soft">{{ __("Submit document for address verification.") }}</p>
							</div>
							<div class="nk-kyc-step-state d-inline-flex align-items-center">
								@if ($document && $basic && $additional)
								<span class="icon ni {{ $iconCheck }} text-soft"></span>
								@elseif ($started && $basic && $document)
								<span class="icon ni {{ $iconArrow }} text-soft"></span>
								@else
								<span class="icon ni {{ $resubmit ? $iconAlert : $iconInfo }} text-soft nk-tooltip"
									data-placement="left" title="{{ $resubmit ? $require : $pending }}"></span>
								@endif
							</div>
						</div>
					</li>
					@endif
				</ul>
				@if ($resubmit)
				<p class="mt-1"><span class="text-soft fs-13px fw-normal">* {{ __("Resubmit all the necessary document to verify
						your identity.") }}</span></p>
				@endif
			</div>{{-- .nk-kyc-app --}}
			<div class="nk-kyc-app-action mt-4">
				<a class="btn btn-lg btn-wider btn-primary kyc-unext" data-action="proceed">
					<span>{{ ($document && $basic && $additional) ? __("Proceed") : __("Click to Proceed")
						}}</span>
				</a>
			</div>
			<div class="nk-kyc-app-note text-center mt-3">
				<div class="note text-soft small">{!! __("By clicking on proceed, you agree to our :terms", ['sitename' =>
					site_info('name'), 'terms' => get_page_link('terms', '', true) ]) !!}</div>
			</div>
		</div>
	</div>
</div>{{-- .nk-content-body --}}
@endsection

@push('scripts')
<script src="{{ asset('/assets/js/libs/dropzone.js') }}"></script>
<script>
	const vroutes = {proceed: "{{ route('user.kyc.verify.next') }}", basic: "{{ route("user.kyc.verify.basic") }}", docs: "{{ route("user.kyc.verify.documents") }}", proof: "{{ route("user.kyc.verify.additional") }}", upload: "{{ route('user.kyc.verify.files') }}"};
	$('#kyc-step-1').addClass("passed");
	$('#kyc-step-1').addClass("w--current");

    $('.kyc-unext').on('click', function (e) {
    	e.preventDefault();
    	let $self = $(this), action = $self.data("action");

    	if (vroutes && action && vroutes[action] !== null) {
    		CustomApp.Form.toForward(vroutes[action], {confirm: true, method: action}, {btn: $self, container: '#kyc-step-container'});
    	}
    });
</script>
@endpush
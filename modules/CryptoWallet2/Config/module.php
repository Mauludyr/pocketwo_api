<?php

use Modules\CryptoWallet2\CryptoWallet2Module;

return [
    CryptoWallet2Module::SLUG => [
        'name' => __('Crypto Wallet 2'),
        'slug' => CryptoWallet2Module::SLUG,
        'method' => CryptoWallet2Module::METHOD,
        'icon' => 'ni-wallet-fill',
        'full_icon' => 'ni-wallet-fill',
        'is_online' => false,
        'processor_type' => 'payment',
        'processor' => CryptoWallet2Module::class,
        'rounded' => 0,
        'supported_currency' => [
            'BTC', 'ETH', 'LTC', 'BCH', 'BNB', 'ADA', 'XRP', 'USDC', 'USDT', 'TRX'
        ],
        'system' => [
            'kind' => 'Payment',
            'info' => 'Manual / Offline',
            'type' => CryptoWallet2Module::MOD_TYPES,
            'version' => CryptoWallet2Module::VERSION,
            'update' => CryptoWallet2Module::LAST_UPDATE,
            'description' => 'Accept crypto related payments manually from user.',
            'addons' => false,
        ]
    ],
];

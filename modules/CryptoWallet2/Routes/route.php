<?php

use Illuminate\Support\Facades\Route;

Route::name('admin.settings.gateway.payment.')->middleware(['auth', 'admin'])->prefix('admin/settings/gateway/payment-method')->group(function () {
    Route::get('/crypto-wallet-2', 'WalletSettingsController@settingsView')->name('crypto-wallet-2');
    Route::post('/crypto-wallet-2', 'WalletSettingsController@saveWalletSettings')->name('crypto-wallet-2.save');
});

Route::middleware(['user'])->group(function(){
    Route::get('crypto-wallet-2/deposit-complete', 'TransactionConfirmationController@depositComplete')->name('user.crypto.wallet-2.deposit.complete');
    Route::post('crypto-wallet-2/deposit/reference', 'TransactionConfirmationController@saveReference')->name('user.crypto.wallet-2.deposit.reference');
});


<?php

use Modules\WdCrypto\WdCryptoModule;

return [
    WdCryptoModule::SLUG => [
        "name" => __("Crypto Wallet"), 
        "account" => __("Crypto Wallet"), 
        "slug" => WdCryptoModule::SLUG, 
        "method" => WdCryptoModule::METHOD, 
        "icon" => "ni-wallet-fill", 
        "full_icon" => "ni-wallet-fill", 
        "is_online" => false, 
        "processor_type" => "withdraw", 
        "processor" => "Modules\\WdCrypto\\WdCryptoModule", 
        "rounded" => 0, 
        "supported_currency" => [
            "BTC", "ETH", "LTC", "BCH", "BNB", "USDC", "USDT", "TRX", "ADA", "XRP"
        ], 
        "system" => [
            "kind" => "Withdraw", 
            "info" => "Manual / Offline", 
            "type" => WdCryptoModule::MOD_TYPES, 
            "version" => WdCryptoModule::VERSION, 
            "update" => WdCryptoModule::LAST_UPDATE, 
            "description" => "Manage withdraw funds manually via crypto wallet.", 
            "addons" => true
        ]
    ]
];

?>
<?php

namespace Modules\WdCrypto\Controllers;

use App\Rules\Gtmin;
use App\Models\WithdrawMethod;
use App\Enums\WithdrawMethodStatus;
use App\Http\Controllers\Controller;
use Modules\WdCrypto\WdCryptoModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Validation\ValidationException;

class SettingsController extends Controller
{
    private $wdMethod = NULL;
    public function __construct(WdCryptoModule $module)
    {
        $this->wdMethod = $module;
    }
    public function showSetting()
    {
        $config = config('modules.'.WdCryptoModule::SLUG);
        $currencies = $this->wallet_currencies();
        $settings = $this->wdMethod->getModuler();
        $outdated = $this->wdMethod->compatible() ? false : $this->wdMethod->minAppVer();
        return view("WdCrypto::settings", compact("settings", "currencies", "config", "outdated"));
    }
    public function updateSetting(Request $request)
    {
        if(empty($request->slug) || $request->slug !== WdCryptoModule::SLUG) {
            return response()->json([ 'type' => 'error', 'msg' => __('Sorry, something wrong with withdraw method.') ]);
        }
            $input = $request->validate([
                "slug" => "required", 
                "name" => "required|string|max:190", 
                "desc" => "required|string|max:190", 
                "currencies" => "required|array|min:1", 
                "min_amount" => "bail|required|numeric|min:0|regex:/^\\d+(\\.\\d{1,5})?\$/", 
                "max_amount" => [
                    "bail", "required", "numeric", "min:0", new Gtmin($request->min_amount), "regex:/^\\d+(\\.\\d{1,5})?\$/"
                ], 
                "status" => "nullable", 
                "config" => "array", 
                "config.wallet.*.min" => "bail|required|numeric|min:0|regex:/^\\d+(\\.\\d{1,5})?\$/", 
                "config.wallet.*.max" => [
                    "bail", "required", "numeric", "min:0", "regex:/^\\d+(\\.\\d{1,5})?\$/", new Gtmin(data_get($request, "config.meta.min"))
                ]
            ], [
                "slug.required" => __("Sorry, your withdraw method is invalid."), 
                "name.required" => __("Withdraw method title is required."), 
                "desc.required" => __("Withdraw method short description is required."), 
                "currencies.*" => __("Select at-least one currency from supported currencies."), 
                "min_amount.required" => __(":Label is required.", [
                    "label" => __("Minimum Amount")
                ]), 
                "min_amount.numeric" => __(":Label must be numeric.", [
                    "label" => __("Minimum Amount")
                ]), 
                "min_amount.min" => __(":Label must be at least :num.", [
                    "label" => __("Minimum Amount"), "num" => "0"
                ]), 
                "min_amount.regex" => __("Allow only :num digit after decimal point in :label.", [
                    "label" => __("Minimum Amount"), "num" => "5"
                ]), 
                "max_amount.required" => __(":Label is required.", [
                    "label" => __("Maximum Amount")
                ]), 
                "max_amount.numeric" => __(":Label must be numeric.", [
                    "label" => __("Maximum Amount")
                ]), 
                "max_amount.min" => __(":Label must be at least :num.", [
                    "label" => __("Maximum Amount"), "num" => "0"
                ]), 
                "max_amount.regex" => __("Allow only :num digit after decimal point in :label.", [
                    "label" => __("Maximum Amount"), "num" => "5"
                ]), 
                "config.wallet.*.min.required" => __(":Label is required.", [
                    "label" => __("Amount to Withdraw") . " (" . __("Min") . ")"
                ]), 
                "config.wallet.*.min.numeric" => __(":Label must be numeric.", [
                    "label" => __("Amount to Withdraw") . " (" . __("Min") . ")"
                ]), 
                "config.wallet.*.min.min" => __(":Label must be at least :num.", [
                    "label" => __("Amount to Withdraw") . " (" . __("Min") . ")", "num" => "0"
                ]), 
                "config.wallet.*.min.regex" => __("Allow only :num digit after decimal point in :label.", [
                    "label" => __("Amount to Withdraw") . " (" . __("Min") . ")", "num" => "5"
                ]), 
                "config.wallet.*.max.required" => __(":Label is required.", [
                    "label" => __("Amount to Withdraw") . " (" . __("Max") . ")"
                ]), 
                "config.wallet.*.max.numeric" => __(":Label must be numeric.", [
                    "label" => __("Amount to Withdraw") . " (" . __("Max") . ")"
                ]), 
                "config.wallet.*.max.min" => __(":Label must be at least :num.", [
                    "label" => __("Amount to Withdraw") . " (" . __("Max") . ")", "num" => "0"
                ]), 
                "config.wallet.*.max.regex" => __("Allow only :num digit after decimal point in :label.", [
                    "label" => __("Amount to Withdraw") . " (" . __("Max") . ")", "num" => "5"
                ])
            ]);

            $currencies = $this->wallet_currencies();
            foreach ($currencies as $currency) {
                if (in_array($currency["code"], $input["currencies"])) {
                    if ($input["config"]["wallet"][$currency["code"]]["max"] != 0 && $input["config"]["wallet"][$currency["code"]]["max"] <= $input["config"]["wallet"][$currency["code"]]["min"]) {
                        throw ValidationException::withMessages(["config.wallet." . $currency["code"] . ".max" => __("The maximum amount must be greater than minimum amount for :currency", ["currency" => $currency["code"]])]);
                    }
                    $input["config"]["wallet"][$currency["code"]] = array_map("strip_tags_map", $input["config"]["wallet"][$currency["code"]]);
                }
            }
            $input["config"]["meta"] = array_map("strip_tags_map", $input["config"]["meta"]);
            if (isset($input["config"]["meta"]["currency"]) && $input["config"]["meta"]["currency"]) {
                $input["currencies"] = array_unique(array_merge($input["currencies"], [$input["config"]["meta"]["currency"]]));
            }
            $input["status"] = $input["status"] == "active" ? WithdrawMethodStatus::ACTIVE : WithdrawMethodStatus::INACTIVE;
            try {
                return $this->wrapInTransaction(function ($input) {
                    WithdrawMethod::updateOrCreate(["slug" => $input["slug"]], array_map("strip_tags_map", $input));
                    return response()->json(["msg" => __("Withdraw method successfully updated.")]);
                }, $input);
            } catch (ValidationException $e) {
                throw $e;
            } catch (\Exception $e) {
                save_error_log($e);
                throw ValidationException::withMessages(["error" => __("Failed to update withdraw method. Please try again.")]);
            }
    }
    public function wallet_currencies()
    {
        $config = config('modules.'.WdCryptoModule::SLUG);

        $currencies = array_filter(config('currencies'), function ($key) use ($config) {
            return in_array($key, data_get($config, "supported_currency"));
        }, ARRAY_FILTER_USE_KEY);
        return $currencies;
    }
}

?>
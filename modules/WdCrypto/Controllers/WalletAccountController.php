<?php

namespace Modules\WdCrypto\Controllers;

use App\Helpers\CustomHash;
use App\Models\UserAccount;
use App\Models\WithdrawMethod;
use App\Http\Controllers\Controller;
use Modules\WdCrypto\WdCryptoModule;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class WalletAccountController extends Controller
{
    public function form(Request $request)
    {
        if ($request->ajax()) {
            $action = route("user.withdraw.account." . WdCryptoModule::SLUG . ".save");
            $quickAdd = $request->get("quick_added", false);
            $method = $this->getWithdrawMethod();
            $currencies = $method->currencies;
            $fallback = isset($method->currencies[0]) ? $method->currencies[0] : "";
            $default = data_get($method->config, "meta.currency", $fallback);
            return view("WdCrypto::account-form", compact("action", "method", "currencies", "default", "quickAdd"));
        }
    }
    private function getWithdrawMethod()
    {
        return WithdrawMethod::where("slug", WdCryptoModule::SLUG)->first();
    }
    private function validateInput(Request $request)
    {
        return $request->validate(["cwm-address" => "required|string", "cwm-currency" => "required|string", "cwm-label" => "nullable"], ["cwm-address.*" => __("Enter a valid wallet address."), "cwm-currency.*" => __("Please select your wallet.")]);
    }
    public function saveWallet(Request $request)
    {
        $input = $this->validateInput($request);
        $name = $input["cwm-label"] ? strip_tags($input["cwm-label"]) : "AC-" . substr(sprintf("%04s", auth()->user()->id), -4, 4) . "-" . rand(1001, 9999);
        $config = ["currency" => $input["cwm-currency"], "wallet" => $input["cwm-address"]];
        $account = new UserAccount();
        $account->fill(["user_id" => auth()->user()->id, "slug" => WdCryptoModule::SLUG, "name" => $name, "config" => $config]);
        $account->save();
        if ($request->get("quick_added") == "yes") {
            return redirect()->route("withdraw.redirect.amount");
        }
        return response()->json(["msg" => __("Your wallet address successfully added for withdraw."), "msg_title" => __("Account Added")]);
    }
    public function editWallet($hash)
    {
        $id = CustomHash::toID($hash);
        $method = $this->getWithdrawMethod();
        $currencies = $method->currencies;
        $fallback = isset($method->currencies[0]) ? $method->currencies[0] : "";
        $default = data_get($method->config, "meta.currency", $fallback);
        $userAccount = UserAccount::where("id", $id)->where("user_id", auth()->user()->id)->first();
        if (blank($userAccount)) {
            throw ValidationException::withMessages(["acc" => [__("Invalid Wallet"), __("Sorry, wallet may invalid or not found.")]]);
        }
        $action = route("user.withdraw.account." . WdCryptoModule::SLUG . ".update", ["id" => CustomHash::of($id)]);
        $quickAdd = false;
        return view("WdCrypto::account-form", compact("userAccount", "currencies", "default", "action", "quickAdd"));
    }
    public function updateWallet($hash, Request $request)
    {
        $id = CustomHash::toID($hash);
        $input = $this->validateInput($request);
        $name = strip_tags($input["cwm-label"]);
        $config = ["currency" => $input["cwm-currency"], "wallet" => $input["cwm-address"]];
        $account = UserAccount::where("id", $id)->where("slug", WdCryptoModule::SLUG)->where("user_id", auth()->user()->id)->first();
        if (blank($account)) {
            throw ValidationException::withMessages(["acc" => [__("Invalid Account"), __("Sorry, account may invalid or not found.")]]);
        }
        $account->update(["user_id" => auth()->user()->id, "slug" => WdCryptoModule::SLUG, "name" => $name, "config" => $config]);
        return response()->json(["reload" => 1500, "msg" => __("Crypto wallet successfully updated."), "msg_title" => __("Account Updated")]);
    }
    public function deleteWallet($hash)
    {
        $id = CustomHash::toID($hash);
        $account = UserAccount::where("id", $id)->where("slug", WdCryptoModule::SLUG)->where("user_id", auth()->user()->id)->first();
        if (blank($account)) {
            throw ValidationException::withMessages(["acc" => [__("Invalid Account"), __("Sorry, account may invalid or not found.")]]);
        }
        $account->delete();
        return response()->json(["msg" => __("The account successfully deleted."), "msg_title" => __("Account Deleted")]);
    }
}

?>
<?php

use Illuminate\Support\Facades\Route;

Route::name("admin.settings.gateway.withdraw.")->middleware(["admin"])->prefix("admin/settings/gateway/withdraw-method")->group(function () {
    Route::get("/crypto-withdraw", "SettingsController@showSetting")->name("wd-crypto-wallet");
    Route::post("/crypto-withdraw", "SettingsController@updateSetting")->name("wd-crypto-wallet.update");
});
Route::name("user.withdraw.account.")->middleware(["user"])->prefix("withdraw/account")->group(function () {
    Route::get("/crypto-withdraw", "WalletAccountController@form")->name("wd-crypto-wallet.form");
    Route::post("/crypto-withdraw", "WalletAccountController@saveWallet")->name("wd-crypto-wallet.save");
    Route::get("/crypto-withdraw/{id}", "WalletAccountController@editWallet")->name("wd-crypto-wallet.edit");
    Route::post("/crypto-withdraw/{id}/update", "WalletAccountController@updateWallet")->name("wd-crypto-wallet.update");
    Route::post("/crypto-withdraw/{id}/delete", "WalletAccountController@deleteWallet")->name("wd-crypto-wallet.delete");
});

?>
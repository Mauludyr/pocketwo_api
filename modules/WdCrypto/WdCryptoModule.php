<?php

namespace Modules\WdCrypto;

use App\Models\WithdrawMethod;
use App\Interfaces\Withdrawable;
use App\Services\SettingsService;
use App\Enums\WithdrawMethodStatus;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class WdCryptoModule implements Withdrawable
{
    private $bySlug = NULL;
    private $withdrawMethod = NULL;
    const VERSION = "1.1.4";
    const LAST_UPDATE = "01142022";
    const MIN_APP_VER = "1.2.0";
    const MOD_TYPES = "module";
    const SLUG = "wd-crypto-wallet";
    const METHOD = "crypto";

    public function __construct()
    {
        $this->withdrawMethod = WithdrawMethod::where("slug", self::SLUG)->first();
        $this->bySlug = 'wd_crypto_wallet';
    }
    public function getModuler()
    {
        return $this->withdrawMethod;
    }
    public function minAppVer()
    {
        return self::MIN_APP_VER;
    }
    public function getVer()
    {
        return self::VERSION;
    }
    public function getSlug()
    {
        return self::SLUG;
    }
    public function compatible()
    {
        return 0 <= version_compare(config("app.version"), $this->minAppVer()) ? true : false;
    }
    
    public function getTitle()
    {
        $title = data_get($this->withdrawMethod->config, "meta.title");
        return !empty($title) ? $title : $this->withdrawMethod->name;
    }
    public function makeWithdraw($transaction, $methodDetails)
    {
    }
    public function verifyWithdraw($transaction, $methodDetails)
    {
    }
    public function getMethod($name = NULL)
    {
        $name = $name == "method" ? "method" : "account";
        return data_get($this->withdrawMethod->module_config, $name, $this->withdrawMethod->name);
    }
    public function getAccountDetails($account)
    {
        $currency = data_get($account, "config.currency");
        $wallet = data_get($account, "config.wallet");
        return $currency . " / " . str_compact($wallet, "...", 6);
    }
    public function getAccountName($account)
    {
        $wallet = "";
        $address = data_get($account, "config.wallet", false);
        if ($address) {
            $wallet .= str_compact($address, "...", 6);
        }
        return $wallet;
    }
    public function getCurrency($account)
    {
        return data_get($account, "config.currency");
    }
    public function isActive()
    {
        if (!$this->compatible()) {
            return false;
        }
        if (blank($this->withdrawMethod)) {
            return false;
        }
        if (!$this->hasConfig()) {
            return false;
        }
        if (empty($this->withdrawMethod->currencies)) {
            return false;
        }
        return true;
    }
    public function isEnable()
    {
        if (!$this->isActive()) {
            return false;
        }
        if ($this->withdrawMethod->status == WithdrawMethodStatus::ACTIVE) {
            return true;
        }
        return false;
    }
    public function hasConfig()
    {
        return !empty($this->withdrawMethod->config);
    }
    public function getPayInfo($account, $currency = NULL, $only = false)
    {
        if (!$this->hasConfig()) {
            return false;
        }
        if (blank($account)) {
            return false;
        }
        $payinfo = ["method" => $this->getTitle(), "label" => $account->name, "wallet" => data_get($account->config, "wallet"), "currency" => data_get($account->config, "currency")];
        if ($only === true) {
            return $payinfo["wallet"];
        }
        return !empty($payinfo) ? $payinfo : false;
    }
    public function getDetailsView($transaction, $wdm)
    {
        return view("WdCrypto::details", compact("transaction", "wdm"));
    }
    public static function getShortcutInfo($transaction)
    {
        $data = ["Reference ID" => the_tnx($transaction->tnx), "Withdraw Amount" => money($transaction->total, sys_settings("base_currency"), ["dp" => "calc"]), "Withdraw Method" => $transaction->method_name, "Account Transfer To" => data_get($transaction, "meta.account"), "Transfer Amount" => money($transaction->tnx_amount, $transaction->tnx_currency, ["dp" => "calc"])];
        return $data;
    }
}

?>
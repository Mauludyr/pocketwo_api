@extends('admin.layouts.master')
@section('title', __('Transaction List'))

@section('content')
<div class="nk-content-body">
  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between gy-2 gx-3">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">
          Set Interest Rate
        </h3>

      </div>

    </div>
  </div>
  <div class="col-6">
    <form method="post" action={{ route('admin.interest.change') }}>
      {{ csrf_field() }}
      <input type="text" name="interest" class="form-control" value="{{ $int_rate }}"></input>
      <br />
      <button type="submit" class="btn btn-block btn-primary">SUBMIT</button>
    </form>

  </div>
</div>
@endsection

@push('modal')
<div class="modal fade" role="dialog" id="ajax-modal"></div>
@endpush

@push('scripts')
<script type="text/javascript">

</script>
@endpush
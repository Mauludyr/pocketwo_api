@extends('admin.layouts.master')

@php 

$byTypes = (!request('state')) ? false : ucfirst(request('state'));

@endphp

@section('title', __($pageTitle))

@section('content')
    <div class="nk-content-body">
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">{{ $pageTitle }} List</h3>
                    <div class="nk-block-des text-soft">
                        <p>{!! __('Total :number :type account.', ['number' => '<strong class="text-base">'.$offers->total() .'</strong>', 'type' => '<span class="text-base">'. $pageTitle .'</span>']) !!}</p>
                    </div>
                </div>
            </div>
        </div>
        {{-- nk-block-head --}}
        <div class="nk-block">
            @if(filled($offers))
            <div class="card card-bordered card-stretch">
                <div class="card-inner-group">
                    <div class="card-inner position-relative card-tools-toggle"></div>
                    <div class="card-inner p-0">
                        <div class="nk-tb-list nk-tb-ulist{{ user_meta('user_display') == 'compact' ? ' is-compact': '' }}">
                            <div class="nk-tb-item nk-tb-head">
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Email') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Loan Amount') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Provider Name') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Late Charges') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Interest Rate') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Interest Amount') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Repayment Amount') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Status') }}</span></div>
                                <div class="nk-tb-col nk-tb-col-tools">&nbsp;</div>
                            </div>

                            {{-- User list item --}}
                            @foreach($offers as $loan)
                                <div class="nk-tb-item">
                                    <div class="nk-tb-col tb-col-sm"><span>{{ $loan->user->email }}</span></div>
                                    <div class="nk-tb-col tb-col-sm"><span>{{ $loan->loan->loan_amount }}</span></div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->provider_name }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->late_charges }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->interest_rate }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->interest_amount }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->repayment_amount }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm"><span>{{ $loan->status }}</span></div>
                                    <div class="nk-tb-col nk-tb-col-tools">
                                        <ul class="nk-tb-actions gx-1">
                                            <li class="nk-tb-action-hidden">
                                                <a href="{{route('admin.loans.details', ['id' => $loan->id, 'type' => 'offer'])}}" class="btn btn-sm btn-trigger btn-icon"><em class="icon ni ni-eye-fill"></em></a>
                                            </li>
                                            <li class="nk-tb-action-hidden">
                                                <a href="javascript:void(0)" data-method="remove" data-lang="{{ $loan->id }}" class="btn btn-sm btn-trigger btn-icon qma-lang">
                                                    <em class="icon ni ni-trash"></em></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        {{-- User List item-end --}}
                        </div>
                    </div>
                    {{-- Pagination --}}
                    <div class="card-inner">
                        <div class="nk-block-between-md g-3">
                            {{ $offers->appends(request()->all())->links('admin.user.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
            @else 
            <div class="card card-bordered text-center">
                <div class="card-inner card-inner-lg py-5">
                    @if ($pageTitle == 'Borrower')
                        <h4>{{ __("No Borrower Found") }}</h4>
                    @else
                        <h4>{{ __("No Lender Found") }}</h4>
                    @endif
                </div>
            </div>
            @endif
        </div>
    {{-- nk-block --}}
    </div>
@endsection

@push('modal')
    <div class="modal fade" role="dialog" id="ajax-modal"></div>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            const routes = {remove: "{{ route('admin.loans.offer.delete') }}"},
                msgs = {
                    remove: { title: "{{ __('Do you want to delete?') }}", btn: {cancel: "{{ __('Cancel') }}", confirm: "{{ __('Delete') }}"}, context: "{!! __("You cannot revert back this action, so please confirm that you want to delete the :type.", ['type' => __("Loan Offer")]) !!}", custom: "danger", type: "warning" },
                };

            let qmlBtn = '.qma-lang', modal = '#ajax-modal';

            $(qmlBtn).on('click', function(e) {
                e.preventDefault();
                let $this = $(this),
                    method = $this.data('method'), lang = $this.data('lang'),
                    url = routes[method], qmsg = msgs[method],
                    data = (lang) ? { uid: lang } : {};

                if (url) {
                    if (method == 'remove' && qmsg) {
                        CustomApp.Ask(qmsg.title, qmsg.context, qmsg.btn, '', 'info').then(function(confirm){
                            if(confirm) {
                                CustomApp.Form.toAjax(url, data);
                            }
                        });
                    } else {
                        CustomApp.Form.toModal(url, data, { modal: $(modal) });
                    }
                }
            });

            $(document).on('click', qmlBtn + '-save', function(e) {
                let $self = $(this), $form = $self.parents('form'), url = $form.attr('action'), data = $form.serialize(), opt = { btn: $self };
                if(url && data) { CustomApp.Form.toPost(url, data, opt); }
            });
        })

    </script>
@endpush
@extends('admin.layouts.master')

@php 

$byTypes = (!request('state')) ? false : ucfirst(request('state'));

@endphp

@section('title', __($pageTitle))

@section('content')
    <div class="nk-content-body">
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">{{ $pageTitle }} List</h3>
                    <div class="nk-block-des text-soft">
                        <p>{!! __('Total :number :type account.', ['number' => '<strong class="text-base">'.$loans->total() .'</strong>', 'type' => '<span class="text-base">'. $pageTitle .'</span>']) !!}</p>
                    </div>
                </div>
            </div>
        </div>
        {{-- nk-block-head --}}
        <div class="nk-block">
            @if(filled($loans))
            <div class="card card-bordered card-stretch">
                <div class="card-inner-group">
                    <div class="card-inner position-relative card-tools-toggle"></div>
                    <div class="card-inner p-0">
                        <div class="nk-tb-list nk-tb-ulist{{ user_meta('user_display') == 'compact' ? ' is-compact': '' }}">
                            <div class="nk-tb-item nk-tb-head">
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Email') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Loan Amount') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Loan Duration') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Interest Rate') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Loan Repayment') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Loan Interest') }}</span></div>
                                <div class="nk-tb-col tb-col-sm"><span class="sub-text">{{ __('Status') }}</span></div>
                                <div class="nk-tb-col nk-tb-col-tools">&nbsp;</div>
                            </div>

                            {{-- User list item --}}
                            @foreach($loans as $loan)
                                <div class="nk-tb-item">
                                    <div class="nk-tb-col tb-col-sm"><span>{{ $loan->user->email }}</span></div>
                                    <div class="nk-tb-col tb-col-sm"><span>{{ $loan->loan_amount }}</span></div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->loan_duration }} {{ $loan->interest_rate_type }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->min_interest_rate }}-{{ $loan->max_interest_rate }}  {{ $loan->interest_rate_type }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->min_loan_repayment }}-{{ $loan->max_loan_repayment }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <span>{{ $loan->min_loan_interest }}-{{ $loan->max_loan_interest }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-sm"><span>{{ $loan->status }}</span></div>
                                    <div class="nk-tb-col nk-tb-col-tools">
                                        <ul class="nk-tb-actions gx-1">
                                            <li class="nk-tb-action-hidden">
                                                <a href="{{route('admin.loans.details', ['id' => $loan->id, 'type' => 'request'])}}" class="btn btn-sm btn-trigger btn-icon"><em class="icon ni ni-eye-fill"></em></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        {{-- User List item-end --}}
                        </div>
                    </div>
                    {{-- Pagination --}}
                    <div class="card-inner">
                        <div class="nk-block-between-md g-3">
                            {{ $loans->appends(request()->all())->links('admin.user.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
            @else 
            <div class="card card-bordered text-center">
                <div class="card-inner card-inner-lg py-5">
                    @if ($pageTitle == 'Borrower')
                        <h4>{{ __("No Borrower Found") }}</h4>
                    @else
                        <h4>{{ __("No Lender Found") }}</h4>
                    @endif
                </div>
            </div>
            @endif
        </div>
    {{-- nk-block --}}
    </div>
@endsection

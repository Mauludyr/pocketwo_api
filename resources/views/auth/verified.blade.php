@extends('auth.layouts.master-nosidebar')

@section('title', __('Verification Completed'))

@section('content')
<div class="card card-bordered">
    <div class="card-inner card-inner-lg">
        <div class="nk-block-head text-center">
            <h4 class="nk-block-title">{{ __('Thank you for your confirmation!') }}</h4>
        </div>
        <div class="nk-block-content text-center">
            <p><strong>{{ __('') }}</strong></p>
            <p>{{ __("You have successfully verified your email address. You can now login into your account and continue to use our platform.") }}
            <div class="gap gap-md"></div>
            <a class="btn btn-lg btn-block btn-primary d-none" href="{{ route('auth.login.form') }}">{{ __('Login') }}</a>
        </div>
    </div>
</div>
@endsection

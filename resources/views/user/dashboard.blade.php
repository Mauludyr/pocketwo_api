@extends('user.layouts.master')



@section('title', __('Dashboard'))



@section('content')

<div class="nk-content-body">

    <div class="nk-block-head nk-block-head-sm">

        <div class="nk-block-head-sub"><span>{{ __('Welcome!') }}</span></div>

        <div class="nk-block-between-md g-4">

            <div class="nk-block-head-content">

                <h2 class="nk-block-title fw-normal">{{ (auth()->user()->display_name) ? auth()->user()->display_name :
                    auth()->user()->name }}</h2>

                <div class="nk-block-des">

                    <p>{{ __("Here's a summary of your account!") }}</p>

                </div>

            </div>

            <div class="nk-block-head-content d-none d-md-inline-flex">

                <ul class="nk-block-tools gx-3">

                    @if (module_exist('FundTransfer', 'mod') && feature_enable('transfer'))

                    <li><a href="{{ route('user.send-funds.show') }}" class="btn btn-light btn-white"><span>{{ __('Send
                                Funds') }}</span> <em
                                class="icon ni ni-arrow-long-right d-none d-lg-inline-block"></em></a></li>

                    @endif

                    <li><a href="{{ route('deposit') }}" class="btn btn-primary"><span>{{ __('Deposit') }}</span> <em
                                class="icon ni ni-arrow-long-right"></em></a></li>

                </ul>

            </div>

        </div>

    </div>



    @if(has_restriction())

    <div class="nk-block">

        <div class="alert alert-danger bg-white alert-thick">

            <div class="alert-cta flex-wrap flex-md-nowrap g-2">

                <div class="alert-text has-icon">

                    <em class="icon ni ni-report-fill text-danger"></em>

                    <p class="text-base"><strong>{{ __("Caution") }}:</strong> {{ 'All the transactions are NOT real as
                        you have logged into demo application to see the platform.' }}</p>

                </div>

            </div>

        </div>

    </div>

    @endif



    {!! Panel::profile_alerts() !!}



    <div class="nk-block">

        <div class="row g-gs">

            <div class="col-md-4">

                {!! Panel::balance('account', ['cta' => true]) !!}

            </div>

            <div class="col-md-4">

                {!! Panel::balance('deposit') !!}

            </div>

            <div class="col-md-4">

                {!! Panel::balance('withdraw') !!}

            </div>

        </div>

    </div>


    @if (filled($recentTransactions))

    <div class="nk-block nk-block-lg">

        <div class="nk-block-head-sm">

            <div class="nk-block-between-md g-4">

                <div class="nk-block-head-content">

                    <h5 class="nk-block-title title">{{ __('Recent Activity') }}</h5>

                </div>

                <div class="nk-block-head-content">

                    <a href="{{ route('transaction.list') }}">{{ __('See History') }}</a>

                </div>

            </div>

        </div>

        <div class="nk-odr-list card card-bordered">

            @foreach($recentTransactions as $transaction)

            @include('user.transaction.trans-row', compact('transaction'))

            @endforeach

        </div>

    </div>

    @endif



    {!! Panel::referral('invite-card') !!}



    {!! Panel::cards('support') !!}



    @if(Panel::news())

    <div class="nk-block">

        <div class="card card-bordered d-xl-none">

            <div class="card-inner card-inner-sm">

                {!! Panel::news() !!}

            </div>

        </div>

    </div>

    @endif




</div>

@endsection



@if (filled($recentTransactions))

@push('modal')

<div class="modal fade" role="dialog" id="ajax-modal"></div>

@endpush

@endif

@section('custom_js')
<script>
    $(document).ready(function(){
        console.log()
        if(checkMobile())
        {
            $('.btn-i-e').hide()
        }
    })

    var xValues = [
        '{{ date("h:m",strtotime($dateTimeFrame[0] )) }}',
        '{{ date("h:m",strtotime($dateTimeFrame[1] )) }}',
        '{{ date("h:m",strtotime($dateTimeFrame[2] )) }}',
        '{{ date("h:m",strtotime($dateTimeFrame[3] )) }}',
        '{{ date("h:m",strtotime($dateTimeFrame[4] )) }}',
        '{{ date("h:m",strtotime($dateTimeFrame[5] )) }}',
        '{{ date("h:m",strtotime($dateTimeFrame[6] )) }}',
    ]
    var yValues = [
        '{{ $balanceTimeFrame[0] }}',
        '{{ $balanceTimeFrame[1] }}',
        '{{ $balanceTimeFrame[2] }}',
        '{{ $balanceTimeFrame[3] }}',
        '{{ $balanceTimeFrame[4] }}',
        '{{ $balanceTimeFrame[5] }}',
        '{{ $balanceTimeFrame[6] }}',
    ];
    var zValues = [
        '{{ $interestTimeFrame[0] }}',
        '{{ $interestTimeFrame[1] }}',
        '{{ $interestTimeFrame[2] }}',
        '{{ $interestTimeFrame[3] }}',
        '{{ $interestTimeFrame[4] }}',
        '{{ $interestTimeFrame[5] }}',
        '{{ $interestTimeFrame[6] }}',
      
    ];

    var minVal = parseFloat('{{ min($balanceTimeFrame) }}')


    new Chart("myChart", {
    type: "line",
    data: {
        labels: xValues,
        datasets: [{
        fill: true,
        lineTension: 0,
        backgroundColor: "RGBA(255,119,255,0.59)",
        borderColor: "rgba(0,0,255,0.1)",
        data: yValues
        },{
        labels: yValues,
       
        fill: true,
        lineTension: 0,
        backgroundColor: "RGBA(0,125,230,0.46)",
        borderColor: "rgba(0,0,255,0.1)",
        data: zValues
        }]
    },
    options: {
        scales: {
            yAxes: [{
                
                ticks: {
                    min: parseFloat('{{ $currentBalance - $currentInterest }}'),
                    minStepSize: parseFloat('{{ $currentInterest }}'),
                    //maxStepSize: 2,
                    max: parseFloat('{{ $currentBalance + $currentInterest }}')+(parseFloat('{{ $currentInterest }}')*0.5)
                }
            }]
        },
        
        legend: {display: false},

    },
    });

    function checkMobile() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    };
    
</script>
@endsection
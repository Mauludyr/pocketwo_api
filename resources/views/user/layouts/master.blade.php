<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js" id="{{ ghp() }}">

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"> </script>
    <meta charset="utf-8">

    <meta name="author" content="{{ site_info('author') }}">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="site-token" content="{{ site_token() }}">

    <title>@yield('title', 'Dashboard') | {{ site_info('name') }}</title>

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    @php $style = (lang_dir() == 'rtl') ? 'apps.rtl' : 'apps'; @endphp

    <link rel="stylesheet" href="{{ asset('assets/css/'.$style.'.css?') }}">

    @if(sys_settings('ui_theme_skin', 'default')!='default')

    <link rel="stylesheet" href="{{ asset('assets/css/skins/theme-'.sys_settings('ui_theme_skin').'.css?') }}">

    @endif



    @include('misc.analytics')



    @if(sys_settings('header_code'))

    {{ html_string(sys_settings('header_code')) }}

    @endif

</head>

<body class="nk-body npc-cryptlite has-sidebar has-sidebar-fat{{ dark_theme('active') }}" {!! lang_dir()=='rtl'
    ? ' dir="rtl"' : '' !!} data-theme="{{ dark_theme('mode') }}">


    <div class="modal fade" id="modalOtp" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Insert OTP</h4>
                </div>
                <div class="modal-body">
                    <p>Please enter security OTP to continue. Press "Send Email" to start sending OTP to your email.</p>
                    <div class="input-group mb-3">

                        <input type="text" class="form-control otp-field">
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary send-otp-btn" type="button">Send Email</button>
                        </div>
                    </div>
                    {{-- <p>Did not recieved the email ?<span>
                            <p style="text-decoration:underline"><a href="#"><strong>Resend Email</strong></a></p>
                        </span>
                    </p> --}}
                    <br />
                    <button class="btn btn-block btn-primary submit-otp-btn">SUBMIT</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="nk-app-root">

        <div class="nk-main">



            @include('user.layouts.sidebar')



            <div class="nk-wrap">



                @include('user.layouts.header')



                <div class="nk-content nk-content-fluid">

                    <div class="container-xl wide-lg">



                        @include('misc.notices')



                        @yield('content')



                    </div>

                </div>



                @include('user.layouts.footer')



            </div>

        </div>

    </div>



    @include('misc.gdpr')



    @stack('modal')

    @if(sys_settings('custom_stylesheet')=='on')

    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">

    @endif

    @if(dark_theme('exist') && dark_theme('css'))

    <style type="text/css">
        {
                {
                dark_theme('css')
            }
        }
    </style>

    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        })

        var sendCount = 15;

        $(".send-otp-btn").click(function(data){
            $(".send-otp-btn").prop('disabled',true)
            $(".send-otp-btn").text('Please wait...')
             $.ajax({
                url: '/otp/request',
                type: 'post',
                success: function(data){
                    console.log(data)
                    if(data == 1)
                    {
                        
                        const interval = setInterval(() => {
                            if(sendCount > 0)
                            {
                                $(".send-otp-btn").text('Resend ('+sendCount+'s)')
                                sendCount = sendCount-1;
                            }
                            else
                            {
                                $(".send-otp-btn").text('Send Email')
                                sendCount = 30;
                                $(".send-otp-btn").prop('disabled',false)
                                clearInterval(interval);
                            }
                        },1000 );
                    }
                    else
                    {
                        alert('Something wrong. Please try again later.')
                        $(".send-otp-btn").prop('disabled',false)
                    }
                }
            })
        })

        $(".submit-otp-btn").click(function(data){
             $.ajax({
                url: '/otp/check',
                data: {otp: $(".otp-field").val()},
                type: 'post',
                success: function(data){
                    console.log(data)
                    if(data)
                    {   
                        window.location.href = '/withdraw'
                    }
                    else
                    {
                        alert('Wrong OTP')
                    }
                }
            })
        })




    </script>

    <script type="text/javascript">
        const updateSetting = "{{ route('update.setting') }}", upreference = "{{ route('account.preference') }}", getTnxDetails = "{{ route('transaction.details') }}", msgwng = "{{ __("Sorry, something went wrong!") }}", msgunp = "{{ __("Unable to process your request.") }}"{!! (user_consent() === null) ? ', consentURI = "'.route('gdpr.cookie').'"' : '' !!};

    </script>


    <script src="{{ asset('/assets/js/bundle.js?') }}"></script>

    <script src="{{ asset('/assets/js/app.js?') }}"></script>

    <script src="{{ asset('/assets/js/charts.js?') }}"></script>

    <script src="https://cdn.widget.sandbox.xanpool.com/xanpool-sdk-v1.js"></script>
    
    @stack('scripts')

    @if(sys_settings('tawk_api_key'))

    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date(); (function(){ var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0]; s1.async=true; s1.src='https://embed.tawk.to/{{ str_replace(['https://tawk.to/chat/', 'http://tawk.to/chat/'], '', sys_settings('tawk_api_key')) }}'; s1.charset='UTF-8'; s1.setAttribute('crossorigin','*'); s0.parentNode.insertBefore(s1,s0); })();

    </script>

    @endif

    @if(sys_settings('footer_code'))

    {{ html_string(sys_settings('footer_code')) }}

    @endif
    @yield('custom_js')


</body>

</html>
@extends('user.layouts.master')

@section('title', __('Withdraw Funds'))

@section('content')
<div class="nk-content-body select-method">
    <div class="page-dw wide-xs m-auto" id="wds-ajcon">
        {{-- @if (!empty($errors) && is_array($errors))
        <!-- @include('user.transaction.error-state', $errors) -->
        @include('user.transaction.insert-otp', $errors)
        @else --}}
        <div class="nk-pps-apps">
            <div class="nk-pps-steps">
                <span class="step step-1 active"></span>
                <span class="step step-2"></span>
                <span class="step step-3"></span>
                <span class="step step-4"></span>
            </div>
            <div class="nk-pps-title text-center">
                <h3 class="title">{{ __('Withdraw Funds') }}</h3>
                <p class="caption-text">{{ __('We are currently paying USDT/USDC (via TRC20) and SGD (via PayNow)') }}
                </p>
                <p class="sub-text-sm">{{ __('') }}</p>
            </div>
            <form class="nk-pps-form" action="{{ route('withdraw.amount.form') }}" id="wd-method-frm"
                data-required_msg="{{ __('Please choose your withdraw method.') }}">
                <div class="nk-pps-field form-group">
                    <ul class="nk-pm-list" id="wd-option-list">
                        @foreach($activeMethods as $item)
                        <li class="nk-pm-item">
                            <input class="nk-pm-control" type="radio" name="withdraw_method" required
                                value="{{ data_get($item, 'slug') }}" id="{{ data_get($item, 'slug') }}" />
                            <label class="nk-pm-label" for="{{ data_get($item, 'slug') }}">
                                <span class="pm-name">{{ __(data_get($item, 'name')) }}</span>
                                <span class="pm-icon"><em
                                        class="icon ni {{ data_get($item, 'module_config.icon') }}"></em></span>
                            </label>
                        </li>
                        @endforeach
                        <li class="nk-pm-item">
                            <input class="nk-pm-control" type="radio" name="withdraw_method" required value="PayNow"
                                id="paynow" />
                            <label class="nk-pm-label" for="paynow">
                                <span class="pm-name">PayNow</span>
                                <span class="pm-icon">
                                    <image style="height: 15px; width: 25px"
                                        src="{{ asset('images/logo-paynow.png') }}">
                                </span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="nk-pps-field form-group">
                    <div class="form-label-group">
                        <label class="form-label">{{ __('Withdrawing from') }}</label>
                    </div>
                    <input type="hidden" value="{{ AccType('main') }}" name="wd_source" id="nk-pps-source-wdm">
                    <div class="dropdown nk-pps-dropdown">
                        <a href="#" class="dropdown-indicator is-single">
                            <div class="nk-cm-item">
                                <div class="nk-cm-text">
                                    <span class="label fw-bold">{{ w2n(AccType('main')) }}</span>
                                    <span class="desc">{{ __('Available Balance (:amount)', [ 'amount' =>
                                        money($balance, base_currency()) ]) }}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="nk-pps-field btn-go-paynow form-action text-center" style="display:none">
                    <div class="nk-pps-action">
                        <a href="javascript:void(0)" class="btn btn-lg btn-block btn-primary" id="withdraw-now">
                            <span>{{ __('Withdraw Now') }}</span>
                            <span class="spinner-border spinner-border-sm hide" role="status" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>

                <div class="nk-pps-field btn-go-wd-method form-action text-center">
                    <div class="nk-pps-action">
                        <a href="javascript:void(0)" class="btn btn-lg btn-block btn-primary" id="withdraw-now">
                            <span>{{ __('Withdraw Now') }}</span>
                            <span class="spinner-border spinner-border-sm hide" role="status" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>
                {{-- <div class="form-note text-base text-center">{!! __('Check out our withdraw :page.', ['page' => '<a
                        href="#">'.__('processing fees').'</a>']) !!}</div> --}}
            </form>
        </div>
        {{-- @endif --}}
    </div>
</div>

<div class="nk-content-body fill-paynow-wd-data" style="display:none">
    <div class="page-dw wide-xs m-auto" id="wds-ajcon">
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-pps-steps">
                <span class="step step-1"></span>
                <span class="step step-2 active"></span>
                <span class="step step-3"></span>
                <span class="step step-4"></span>
            </div>
            <div class="nk-pps-title text-center">
                <h3 class="title">{{ __('Withdraw Funds') }}</h3>
                <p class="caption-text">{{ __('via PayNow') }}</p>
                <p class="sub-text-sm">{{ __('Your withdrawal will be sent to your PayNow number.') }}</p>
            </div>
        </div>

        <div class="card-inner">
            <div class="row">
                <div class="col-12">
                    <div class="form-set wide-md">
                        <div class="row gy-3">
                            <div class="col-sm-12 col-xxl-12">
                                <div class="form-group">
                                    <div class="row gx-gs gy-2">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="form-label">{{ __('Withdrawal Amount')
                                                    }}</span><small><sup> </sup></small></label>
                                                <div class="form-control-wrap">
                                                    <div class="form-text-hint"><span>{{ base_currency()
                                                            }}</span></div>
                                                    <input type="number" class="form-control amount-paynow"
                                                        name="min_amount" value="" min="0">
                                                </div>
                                                <div class="form-note">Current balance <strong>{{
                                                        money($balance, base_currency()) }}</strong></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="form-set wide-md">
                        <div class="row gy-3">
                            <div class="col-sm-12 col-xxl-12">
                                <div class="form-group">
                                    <div class="row gx-gs gy-2">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="form-label">{{ __('PayNow Number')
                                                    }}</span><small><sup> </sup></small></label>
                                                <div class="form-control-wrap">

                                                    <input type="text" class="form-control paynow-number"
                                                        name="min_amount" value="" min="0">
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-set wide-md">
                        <div class="row gy-3">
                            <div class="col-sm-12 col-xxl-12">
                                <div class="form-group">
                                    <div class="row gx-gs gy-2">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="form-label">{{ __('Confirm PayNow Number')
                                                    }}</span><small><sup> </sup></small></label>
                                                <div class="form-control-wrap">

                                                    <input type="text" class="form-control confirm-paynow-number"
                                                        name="min_amount" value="" min="0">
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="divider"></div>
                    @csrf
                    <div class="nk-pps-field form-action text-center">
                        <div class="nk-pps-action">
                            <a href="javascript:void(0)" class="btn btn-lg btn-block btn-primary btn-continue"
                                id="continue-withdraw">
                                <span>{{ __('Continue to Withdrawal') }}</span>
                                <span class="spinner-border spinner-border-sm hide" role="status"
                                    aria-hidden="true"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="nk-content-body summary-paynow-wd" style="display:none">
    <div class="page-dw wide-xs m-auto" id="wds-ajcon">
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-pps-steps">
                <span class="step step-1"></span>
                <span class="step step-2"></span>
                <span class="step step-3"></span>
                <span class="step step-4 active"></span>
            </div>
            <div class="nk-pps-title text-center">
                <h3 class="title">{{ __('Confirm Your Withdrawal') }}</h3>
                <p class="caption-text wd-detail">You are about to withdraw</p>
                <p class="sub-text-sm">{{ __('Please review the details and confirm.') }}</p>
            </div>
        </div>

        <div class="card-inner">
            <div class="row">
                <div class="col-12">

                    <div class="nk-pps-field form-action text-center">
                        <div class="nk-pps-action">
                            <a href="javascript:void(0)" class="btn btn-lg btn-block btn-primary btn-submit"
                                data-url="{{ route('withdraw.confirm') }}" id="">
                                <span>{{ __('Confirm & Withdraw') }}</span>
                                <span class="spinner-border spinner-border-sm hide" role="status"
                                    aria-hidden="true"></span>
                            </a>
                        </div>
                        <div class="nk-pps-action pt-3">
                            <a href="{{ route('withdraw') }}" class="btn btn-outline-danger btn-trans">{{ __('Cancel
                                Withdraw') }}</a>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>

<div class="nk-pps-apps wd-end" style="display:none">
    {{-- <div class="nk-pps-steps">
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step active"></span>
    </div> --}}
    <div class="nk-pps-result">
        <em class="icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
        <h3 class="title">{{ __('Your funds are on the way!') }}</h3>
        <div class="nk-pps-text md">
            <p class="caption-text">{{ __("We'll send you a confirmation email shortly. Your withdrawal will be
                processed within 24 hours.") }}</p>
            {{-- <p class="sub-text">{{ __('Your withdrawal request ID :tnx', ['tnx' => the_tnx(data_get($transaction,
                'tnx'))]) }}</p> --}}
        </div>
        <div class="nk-pps-action">
            <ul class="btn-group-vertical align-center gy-3">
                <li><a href="{{ route('withdraw') }}" class="btn btn-lg btn-mw btn-primary">{{ __('Make Another
                        Withdrawal') }}</a></li>
                <li><a href="{{ route('transaction.list') }}" class="link link-primary">{{ __('Check status in
                        Transactions') }}</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection

@push('modal')
<div class="modal fade" role="dialog" id="withdraw-account-modal">
</div>
@endpush
@section('custom_js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    })

    $(".btn-submit").click(function(){
        $(this).prop('disabled',true)
        $.ajax({
            url: '/withdraw/confirm/paynow',
            data: {amount: $(".amount-paynow").val(), _token: '{{ csrf_token() }}', pay_to: $(".paynow-number").val()},
            type: 'post',
            success: function(data)
            {
                $(".btn-submit").prop('disabled',false)
                $(".summary-paynow-wd").hide()
                $(".wd-end").show()
            }
        })
    })

    $(".btn-go-paynow").hide()

    $(".btn-continue").click(function(){
        if(parseFloat($(".amount-paynow").val()) > {{ $balance }})
        {
            alert('Insufficient funds!')
        }
        else if(!$(".amount-paynow").val())
        {
            alert('Please fill withdrawal amount')
        }
        else if(!$(".paynow-number").val())
        {
            alert('Please fill your PayNow number before submitting.')
        }
        else if($(".paynow-number").val() != $(".confirm-paynow-number").val())
        {
            alert('PayNow number does not match. ')
        }
        else if(parseFloat($(".amount-paynow").val()) < 100)
        {
            alert('The minimum amount of 100.00 USDT is required to withdraw')
        }
        else if(parseFloat($(".amount-paynow").val()) > 700)
        {
            alert('The maximum amount for PayNow withdrawal is 700.00 USDT')
        }
        else
        {
            $(".fill-paynow-wd-data").hide()
            $(".wd-detail").text(`You are about to withdraw the SGD equivalent of ` +$(".amount-paynow").val()+ ` {{ base_currency() }} to your PayNow (`+$(".paynow-number").val()+`), using XanPool's prevailing market rate`)
            $(".summary-paynow-wd").show()
        }
      
    })
    // $("input[name='name']:checked").val()
    $("input[name='withdraw_method']").click(function(){
        console.log($("input[name='withdraw_method']:checked").val());
        if($("input[name='withdraw_method']:checked").val() == 'PayNow')
        {
            $(".btn-go-paynow").show()
            $(".btn-go-wd-method").hide()
        }
        else
        {
            $(".btn-go-paynow").hide()
            $(".btn-go-wd-method").show()
        }
    })

    $(".btn-go-paynow").click(function(){
        $(".fill-paynow-wd-data").show()
        $(".select-method").hide()
    })
</script>
@endsection